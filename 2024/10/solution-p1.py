import argparse
import logging

# Configure logging to output to both console and a file
logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[logging.FileHandler("output.log"), logging.StreamHandler()],
)
ROW = 0
COL = 1

def get_valid_neighbors(current, topo):
    logging.debug(f"current: {current}")
    valid_neighbors = []
    if current[ROW]-1 >= 0:
        # up
        valid_neighbors.append([current[ROW]-1, current[COL]])
    if current[ROW]+1 < len(topo[0]):
        # down
        valid_neighbors.append([current[ROW]+1, current[COL]])
    if current[COL]+1 < len(topo):
        # right
        valid_neighbors.append([current[ROW], current[COL]+1])
    if current[COL]-1 >= 0:
        # left
        valid_neighbors.append([current[ROW], current[COL]-1])

    logging.debug(f"valid_neighbors: {valid_neighbors}")
    real_valid_neighbors = []
    for neighbor in valid_neighbors:
        logging.debug(f"{neighbor}")
        if topo[neighbor[ROW]][neighbor[COL]] > topo[current[ROW]][current[COL]]+1:
            continue
        if topo[neighbor[ROW]][neighbor[COL]] <= topo[current[ROW]][current[COL]]:
            continue
        real_valid_neighbors.append(neighbor)
    logging.debug(f"valid_neighbors: {real_valid_neighbors}")
    return real_valid_neighbors



def process_input(raw_input):
    topo = []
    for index, line in raw_input:
        line = line.rstrip()
        logging.debug(f"reading line: {line}")
        row = []
        for char in line:
            row.append(int(char))
        topo.append(row)
    return topo

def findPaths(graph, topo, key_to_pos_map, origin):
    stack  = [(origin,[origin])] # Start from origin
    while stack:
        node, path = stack.pop(-1)                 # DFS (use pop(0) for BFS)
        for nextNode in graph[node]:               # Go through neighbours
            if nextNode in path: continue          # don't loop inside paths 
            nextPath = path + [nextNode]           # Record path to get there
            if get_value_from_node(key_to_pos_map, topo, nextNode) == 9:
                yield nextPath                     # Return this path, 
                continue                           #   continue with others
            stack.append( (nextNode,nextPath) )    # Stack for more exploration

def get_pos_from_node(key_to_pos_map, node):
    return key_to_pos_map.get(node)

def get_value_from_node(key_to_pos_map, topo, node):
    position = get_pos_from_node(key_to_pos_map, node)
    return topo[position[ROW]][position[COL]]

def main():
    # Set up command line argument parsing
    parser = argparse.ArgumentParser(description="Process lines from an input file.")
    parser.add_argument("input_file", type=str, help="Path to the input file")

    args = parser.parse_args()

    logging.info(f"Reading lines from file: {args.input_file}")

    lines = []
    try:
        # Open the input file
        with open(args.input_file, "r") as file:
            # Read and process each line
            topo = process_input(enumerate(file))
    except FileNotFoundError:
        logging.error(f"File not found: {args.input_file}")
    logging.info("")

    # Do work here

    connected_graph = {}
    key_to_pos_map = {}
    list_of_start_nodes = []
    count = -1
    for row_index, row in enumerate(topo):
        for col_index, col in enumerate(row):
            count += 1
            key_to_pos_map[count] = [row_index, col_index]
            if col == 0:
                list_of_start_nodes.append(count)
    count = -1
    for row_index, row in enumerate(topo):
        for col_index, col in enumerate(row):
            count += 1
            valid_neighbors = get_valid_neighbors([row_index, col_index], topo)
            keyed_neighbors = []
            for valid_neighbor in valid_neighbors:
                logging.debug(f"appending {list(key_to_pos_map.keys())[list(key_to_pos_map.values()).index(valid_neighbor)]}")
                keyed_neighbors.append(list(key_to_pos_map.keys())[list(key_to_pos_map.values()).index(valid_neighbor)])
            connected_graph[count] = keyed_neighbors
    logging.debug(f"connected_graph:{connected_graph}")
    logging.debug(f"key_to_node: {key_to_pos_map}")
    logging.debug(f"trailheads: {list_of_start_nodes}")
    total_paths = 0
    for node in list_of_start_nodes:
        logging.info(f"Trailhead {node}")
        visited_ends = []
        for path in findPaths(connected_graph, topo, key_to_pos_map, node):
            if path[-1] not in visited_ends:
                total_paths += 1
                logging.debug(f"found new 9 {path[-1]}")
                logging.debug(f"{path}")
                visited_ends.append(path[-1])
        logging.info(f"total score: {total_paths}")
    logging.info(f"Answer: {total_paths}")


if __name__ == "__main__":
    main()