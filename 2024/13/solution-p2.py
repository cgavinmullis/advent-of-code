import argparse
import logging
import numpy as np
from math import isclose

# Configure logging to output to both console and a file
logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[logging.FileHandler("output.log"), logging.StreamHandler()],
)

def process_input(raw_input):
    next_matrix = []
    prize_matrix = []
    matrix_dict = {}
    count = 0
    coeff_x1 = coeff_x2 = coeff_y1 = coeff_y2 = 0
    for index, line in raw_input:
        line = line.rstrip()
        logging.debug(f"reading line: {line}")
        if "Button A:" in line:
            coeff_x1 = int(line.split(",")[0].split("+")[1])
            coeff_y1 = int(line.split(",")[1].split("+")[1])
            logging.debug(f"{coeff_x1}x + {coeff_y1}y = ?")
        elif "Button B:" in line:
            coeff_x2 = int(line.split(",")[0].split("+")[1])
            coeff_y2 = int(line.split(",")[1].split("+")[1])
            logging.debug(f"{coeff_x2}x + {coeff_y2}y = ?")
            next_matrix.append([coeff_x2, coeff_y2])
        elif "Prize:" in line:
            goal_x = int(line.split(",")[0].split("=")[1]) + 10000000000000
            goal_y = int(line.split(",")[1].split("=")[1]) + 10000000000000
            movement_matrix = [[coeff_x1, coeff_x2],[coeff_y1, coeff_y2]]
            prize_matrix = [[goal_x], [goal_y]]
            logging.debug(f"Goal X: {goal_x}, Goal Y: {goal_y}")
            matrix_dict[count] = {
                "movement_matrix": np.array(movement_matrix),
                "prize_matrix": np.array(prize_matrix),
            }
            count += 1
        else:
            # Blank line
            continue
    return matrix_dict

def calculate_total_token_cost(a_presses, b_presses):
    return a_presses* 3 + b_presses * 1

def is_float(value):
    logging.debug(f"value: {value} {round(value)}")
    if abs(value - round(value)) > 0.01:
        return True
    return not isclose(value, round(value), abs_tol=0.000001)



def main():
    # Set up command line argument parsing
    parser = argparse.ArgumentParser(description="Process lines from an input file.")
    parser.add_argument("input_file", type=str, help="Path to the input file")

    args = parser.parse_args()

    logging.info(f"Reading lines from file: {args.input_file}")

    matrix_dict = {}
    try:
        # Open the input file
        with open(args.input_file, "r") as file:
            # Read and process each line
            matrix_dict = process_input(enumerate(file))
    except FileNotFoundError:
        logging.error(f"File not found: {args.input_file}")
    logging.info("")

    # Do work here

    running_token_total = 0
    for key, challenge in matrix_dict.items():
        movement_matrix = challenge["movement_matrix"]
        prize_matrix = challenge["prize_matrix"]
        logging.debug(f"coeff matrix:\n{movement_matrix}")
        movement_inverse = np.linalg.inv(movement_matrix)
        logging.debug(f"movement_inverse:\n{movement_inverse}")
        logging.debug(f"prize matrix:\n{prize_matrix}")
        solution = np.dot(movement_inverse,prize_matrix)
        a_presses = solution[0][0]
        b_presses = solution[1][0]

        if is_float(a_presses) or is_float(b_presses):
            logging.info(f"invalid float: {a_presses} {b_presses}")
            #invalid
            continue
        test_0 = round(a_presses) * movement_matrix[0][0] + round(b_presses) * movement_matrix[0][1] 
        if not isclose(test_0, prize_matrix[0][0], abs_tol=0.0000001):
            logging.info(f"invalid solution: {a_presses} {b_presses}")
            logging.info(f"test: {test_0} != {prize_matrix[0][0]}")
            # invalid solution
            continue

        test_1 = round(a_presses) * movement_matrix[1][0] + round(b_presses) * movement_matrix[1][1]
        if not isclose(test_1, prize_matrix[1][0], abs_tol=0.0000001):
            logging.info(f"invalid solution: {a_presses} {b_presses}")
            logging.info(f"test: {test_1} != {prize_matrix[1][0]}")
            # invalid solution
            continue
        logging.info(f"solution: {a_presses} A presses and {b_presses} B presses")
        total_token_cost = calculate_total_token_cost(a_presses, b_presses)
        logging.debug(f"total token cost: {total_token_cost} {int(total_token_cost)} {round(total_token_cost)}")
        running_token_total += round(total_token_cost)
    logging.info(f"Answer: {running_token_total}")


if __name__ == "__main__":
    main()