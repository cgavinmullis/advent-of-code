import argparse
import logging

# Configure logging to output to both console and a file
logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[logging.FileHandler("output.log"), logging.StreamHandler()],
)

def process_input(raw_input):
    page_ordering_rules = []
    updates = []
    parsing_ordering_rules = True
    for index, line in raw_input:
        line = line.rstrip()
        logging.debug(f"reading line: {line}")
        if not parsing_ordering_rules:
            updates.append(line.split(","))
        if line == "":
            parsing_ordering_rules = False
        if parsing_ordering_rules:
            page_ordering_rules.append(line.split("|"))
    return page_ordering_rules, updates

def find_middle_page_number(update):
    return update[len(update) // 2]

def check_update_is_valid(page_ordering_rules, update):
    seen_pages = []
    for page in update:
        for rule in page_ordering_rules:
            if page == rule[0]:
                if rule[1] in seen_pages:
                    logging.debug(f"{page}, rule {rule}")
                    return False
        seen_pages.append(page)
    return True

def main():
    # Set up command line argument parsing
    parser = argparse.ArgumentParser(description="Process lines from an input file.")
    parser.add_argument("input_file", type=str, help="Path to the input file")

    args = parser.parse_args()

    logging.info(f"Reading lines from file: {args.input_file}")

    page_ordering_rules, updates = [], []
    try:
        # Open the input file
        with open(args.input_file, "r") as file:
            # Read and process each line
            page_ordering_rules, updates = process_input(enumerate(file))
    except FileNotFoundError:
        logging.error(f"File not found: {args.input_file}")
    logging.info("")

    # Do work here
    for rule in page_ordering_rules:
        logging.debug(f"{rule}")

    total = 0
    for update in updates:
        logging.debug(f"{update}")
        if check_update_is_valid(page_ordering_rules, update):
            logging.debug(f"Middle page is {find_middle_page_number(update)}")
            total += int(find_middle_page_number(update))

    logging.info(f"Answer: {total}")


if __name__ == "__main__":
    main()