import argparse
import logging

# Configure logging to output to both console and a file
logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[logging.FileHandler("output.log"), logging.StreamHandler()],
)

def process_input(raw_input):
    test_values = []
    numbers = []
    for index, line in raw_input:
        line = line.rstrip()
        logging.debug(f"reading line: {line}")
        split_line = line.split(":")
        test_values.append(int(split_line[0]))
        split_line = split_line[1].split(" ")
        del split_line[0]
        line_numbers = []
        for number in split_line:
            line_numbers.append(int(number))
        numbers.append(line_numbers)
    return test_values, numbers

def is_valid_two(test_value, num1, num2):
    logging.debug(f"Final test: {test_value} / {num1} == {num2}?")
    if test_value / num1 == num2:
        logging.debug(f"Found solution: {test_value} / {num1} == {num2}")
        return True
    logging.debug(f"Final test: {test_value} - {num1} - {num2} == 0?")
    if test_value - num1 - num2 == 0:
        logging.debug(f"Found solution: {test_value} - {num1} - {num2} == 0")
        return True
    return False

def is_valid(test_value, number_list):
    if len(number_list) == 2:
        if is_valid_two(test_value, number_list[0], number_list[1]):
            return True
        return False
    else:
        logging.debug(f"Testing: {test_value} / {number_list[-1]}. {number_list[:-1]}")
        if is_valid(test_value / number_list[-1], number_list[:-1]):
            return True
        logging.debug(f"Testing: {test_value} - {number_list[-1]}. {number_list[:-1]}")
        if is_valid(test_value - number_list[-1], number_list[:-1]):
            return True
    return False



def main():
    # Set up command line argument parsing
    parser = argparse.ArgumentParser(description="Process lines from an input file.")
    parser.add_argument("input_file", type=str, help="Path to the input file")

    args = parser.parse_args()

    logging.info(f"Reading lines from file: {args.input_file}")

    test_values = []
    numbers = []
    try:
        # Open the input file
        with open(args.input_file, "r") as file:
            # Read and process each line
            test_values, numbers = process_input(enumerate(file))
    except FileNotFoundError:
        logging.error(f"File not found: {args.input_file}")
    logging.info("")

    # Do work here
    logging.debug(f"{numbers}")
    total_calibration_result = 0
    for test_value, number_list in zip(test_values, numbers):
        if is_valid(test_value, number_list):
            total_calibration_result += test_value

    logging.info(f"Answer: {total_calibration_result}")


if __name__ == "__main__":
    main()