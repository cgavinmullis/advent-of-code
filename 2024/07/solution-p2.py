import argparse
import logging

# Configure logging to output to both console and a file
logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[logging.FileHandler("output.log"), logging.StreamHandler()],
)

def process_input(raw_input):
    test_values = []
    numbers = []
    for index, line in raw_input:
        line = line.rstrip()
        logging.debug(f"reading line: {line}")
        split_line = line.split(":")
        test_values.append(int(split_line[0]))
        split_line = split_line[1].split(" ")
        del split_line[0]
        line_numbers = []
        for number in split_line:
            line_numbers.append(int(number))
        numbers.append(line_numbers)
    return test_values, numbers

def desquish(num, split_index):
    return str(num)[:split_index], str(num)[split_index:]


def is_valid_two(test_value, num1, num2):
    logging.debug(f"Final test: {test_value} / {num1} == {num2}?")
    if test_value / num1 == num2:
        logging.debug(f"Found solution: {test_value} / {num1} == {num2}")
        return True
    logging.debug(f"Final test: {test_value} - {num1} - {num2} == 0?")
    if test_value - num1 - num2 == 0:
        logging.debug(f"Found solution: {test_value} - {num1} - {num2} == 0")
        return True
    for i in range(len(str(test_value))):
        if i == 0:
            continue
        if i == len(str(test_value)):
            continue
        possible_num1, possible_num2 = desquish(int(test_value), i)
        if possible_num1 == "" or possible_num2 == "":
            return False
        logging.debug(f"Final test: {possible_num1} || {possible_num2} == {test_value}?")
        if int(possible_num1) == num1 and int(possible_num2) == num2:
            logging.debug(f"Found solution: {possible_num1} || {possible_num2} == {test_value}?")
            return True
    return False

def is_valid(test_value, number_list):
    logging.debug(f"{test_value}")
    if test_value < 0:
        return False
    if len(number_list) == 2:
        if is_valid_two(test_value, number_list[0], number_list[1]):
            return True
        return False
    else:
        logging.debug(f"Testing: {test_value} / {number_list[-1]}. {number_list[:-1]}")
        if test_value % number_list[-1] == 0:
            if is_valid(test_value // number_list[-1], number_list[:-1]):
                return True
        logging.debug(f"Testing: {test_value} - {number_list[-1]}. {number_list[:-1]}")
        if is_valid(test_value - number_list[-1], number_list[:-1]):
            return True
        logging.debug(f"Testing: {test_value} ~|| {number_list[-1]}. {number_list[:-1]}")
        if str(number_list[-1]) in str(test_value):
            start_index = str(test_value).rfind(str(number_list[-1]))
            if start_index < 0:
                return False
            logging.debug(f"{start_index} {len(str(number_list[-1]))} {len(str(test_value))}")
            if start_index + len(str(number_list[-1])) != len(str(test_value)):
                return False
            num1, num2 = desquish(test_value, start_index)
            logging.debug(f"num2: {num2} == {number_list[0]}?")
            logging.debug(f"num1: {num1}")
            if num1 == "":
                return False
            if str(num2) in str(test_value):
                logging.debug(f"num2: {num2} in {test_value}!")
                if is_valid(int(num1), number_list[:-1]):
                    return True
    return False



def main():
    # Set up command line argument parsing
    parser = argparse.ArgumentParser(description="Process lines from an input file.")
    parser.add_argument("input_file", type=str, help="Path to the input file")

    args = parser.parse_args()

    logging.info(f"Reading lines from file: {args.input_file}")

    test_values = []
    numbers = []
    try:
        # Open the input file
        with open(args.input_file, "r") as file:
            # Read and process each line
            test_values, numbers = process_input(enumerate(file))
    except FileNotFoundError:
        logging.error(f"File not found: {args.input_file}")
    logging.info("")

    # Do work here
    logging.debug(f"{numbers}")
    total_calibration_result = 0
    for test_value, number_list in zip(test_values, numbers):
        if is_valid(test_value, number_list):
            total_calibration_result += test_value
            logging.info(f"{test_value}")
        else:
            logging.debug(f"INVALID: {test_value}: {number_list}")

    logging.debug(f"Answer: {total_calibration_result}")


if __name__ == "__main__":
    main()