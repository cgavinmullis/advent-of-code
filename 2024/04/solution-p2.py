import argparse
import logging
from scipy.ndimage import rotate
import numpy as np

# Configure logging to output to both console and a file
logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[logging.FileHandler("output.log"), logging.StreamHandler()],
)

def process_input(raw_input):
    graph = []
    for index, line in raw_input:
        line = line.rstrip()
        logging.debug(f"reading line: {line}")
        row = []
        for char in line:
            row.append(char)
        graph.append(row)
    
    return graph

def rotate_2d_array_90(graph):
    return list(zip(*reversed(graph)))

def pretty_print_graph(graph):
    for row in graph:
        logging.debug(f"{row}")

def find_xmas_from_a(graph):
    count = 0
    row_max_index = len(graph) - 1
    for row_index, row in enumerate(graph):
        char_max_index = len(row) - 1
        for char_index, char in enumerate(row):
            if (char_index + 1 > char_max_index) or (row_index + 1 > row_max_index):
                continue
            if (char_index - 1 < 0) or (row_index - 1 < 0):
                continue
            if char != "A":
                continue
            # We have an A
            looking_for = ["M", "S"]
            # Check top left to bottom right
            if graph[row_index - 1][char_index - 1] not in looking_for:
                continue
            else:
                looking_for.remove(graph[row_index - 1][char_index - 1])
            if graph[row_index + 1][char_index + 1] not in looking_for:
                continue
            # We have one diag
            looking_for = ["M", "S"]
            # Check top right to bottom left
            if graph[row_index - 1][char_index + 1] not in looking_for:
                continue
            else:
                looking_for.remove(graph[row_index - 1][char_index + 1])
            if graph[row_index + 1][char_index - 1] not in looking_for:
                continue
            # Found X-MAS!
            logging.debug(f"\n")
            logging.debug(f"{graph[row_index - 1][char_index - 1]} {graph[row_index - 1][char_index]} {graph[row_index - 1][char_index + 1]}")
            logging.debug(f"{graph[row_index][char_index - 1]} {graph[row_index][char_index]} {graph[row_index][char_index + 1]}")
            logging.debug(f"{graph[row_index + 1][char_index - 1]} {graph[row_index + 1][char_index]} {graph[row_index + 1][char_index + 1]}")
            count += 1
    return count

def main():
    # Set up command line argument parsing
    parser = argparse.ArgumentParser(description="Process lines from an input file.")
    parser.add_argument("input_file", type=str, help="Path to the input file")

    args = parser.parse_args()

    logging.info(f"Reading lines from file: {args.input_file}")

    lines = []
    try:
        # Open the input file
        with open(args.input_file, "r") as file:
            # Read and process each line
            graph = process_input(enumerate(file))
    except FileNotFoundError:
        logging.error(f"File not found: {args.input_file}")
    logging.info("")

    # Do work here
    count = 0
    pretty_print_graph(list(graph))
    count += find_xmas_from_a(graph)

    logging.info(f"Answer: {count}")


if __name__ == "__main__":
    main()