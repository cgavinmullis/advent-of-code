import argparse
import logging
from scipy.ndimage import rotate
import numpy as np

# Configure logging to output to both console and a file
logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[logging.FileHandler("output.log"), logging.StreamHandler()],
)

def process_input(raw_input):
    graph = []
    for index, line in raw_input:
        line = line.rstrip()
        logging.debug(f"reading line: {line}")
        row = []
        for char in line:
            row.append(char)
        graph.append(row)
    
    return graph

def rotate_2d_array_90(graph):
    return list(zip(*reversed(graph)))

def pretty_print_graph(graph):
    for row in graph:
        logging.debug(f"{row}")

def find_xmas_horizontal(graph):
    count = 0
    for row in graph:
        row_max_index = len(row) - 1
        for index, char in enumerate(row):
            if index + 3 > row_max_index:
                continue
            if char != "X":
                continue
            if row[index + 1] != "M":
                continue
            if row[index + 2] != "A":
                continue
            if row[index + 3] != "S":
                continue
            # Found horizontal!
            logging.debug(f"FOUND IN {row} at {index}")
            count += 1
    return count

def find_xmas_diagonal(graph):
    count = 0
    col_max_index = len(graph) - 1
    for col_index, row in enumerate(graph):
        row_max_index = len(row) - 1
        for index, char in enumerate(row):
            if (index + 3 > row_max_index) or (col_index + 3 > col_max_index):
                continue
            if char != "X":
                continue
            if graph[col_index + 1][index + 1] != "M":
                continue
            if graph[col_index + 2][index + 2] != "A":
                continue
            if graph[col_index + 3][index + 3] != "S":
                continue
            # Found horizontal!
            logging.debug(f"FOUND IN {col_index} at {index}")
            count += 1
    return count

def main():
    # Set up command line argument parsing
    parser = argparse.ArgumentParser(description="Process lines from an input file.")
    parser.add_argument("input_file", type=str, help="Path to the input file")

    args = parser.parse_args()

    logging.info(f"Reading lines from file: {args.input_file}")

    lines = []
    try:
        # Open the input file
        with open(args.input_file, "r") as file:
            # Read and process each line
            graph = process_input(enumerate(file))
    except FileNotFoundError:
        logging.error(f"File not found: {args.input_file}")
    logging.info("")

    # Do work here
    count = 0
    pretty_print_graph(list(graph))
    count += find_xmas_horizontal(graph)
    count += find_xmas_diagonal(graph)
    logging.info("")
    graph = rotate_2d_array_90(graph)
    count += find_xmas_horizontal(graph)
    count += find_xmas_diagonal(graph)
    logging.info("")
    graph = rotate_2d_array_90(graph)
    count += find_xmas_horizontal(graph)
    count += find_xmas_diagonal(graph)
    logging.info("")
    graph = rotate_2d_array_90(graph)
    count += find_xmas_horizontal(graph)
    count += find_xmas_diagonal(graph)

    logging.info(f"Answer: {count}")


if __name__ == "__main__":
    main()