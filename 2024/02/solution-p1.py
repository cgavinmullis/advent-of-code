import argparse
import logging

# Configure logging to output to both console and a file
logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[logging.FileHandler("output.log"), logging.StreamHandler()],
)

def is_safe(record):

    increasing = False
    decreasing = False
    for index, level in enumerate(record):
        if index == 0:
            continue
        left = int(record[index - 1])
        right = int(level)
        difference = left - right
        if difference > 0:
            decreasing = True
        elif difference < 0:
            increasing = True
        else:
            # numbers are equal, record is unsafe
            logging.debug(f"Unsafe equal nums: {left} and {right}")
            return False
        if decreasing and increasing:
            # the slope has changed directions
            logging.debug("Unsafe changed slope")
            return False
        if not are_adjacent_levels_safe(left, right):
            logging.debug(f"Unsafe adjacent distance: {left} and {right}")
            return False
    logging.debug("Safe")
    return True

def are_adjacent_levels_safe(left, right):
    distance = abs(left - right)

    if distance > 3 or distance < 1:
        return False
    
    return True

def process_input(raw_input):
    records = {}
    for index, line in raw_input:
        line = line.rstrip()
        logging.debug(f"reading line: {line}")
        records[index] = line.split()

    logging.debug(f"records: {records}")
    return records

def main():
    # Set up command line argument parsing
    parser = argparse.ArgumentParser(description="Process lines from an input file.")
    parser.add_argument("input_file", type=str, help="Path to the input file")

    args = parser.parse_args()

    logging.info(f"Reading lines from file: {args.input_file}")

    records = {}
    try:
        # Open the input file
        with open(args.input_file, "r") as file:
            # Read and process each line
            records = process_input(enumerate(file))
    except FileNotFoundError:
        logging.error(f"File not found: {args.input_file}")
    logging.info("")

    # Do work here
    safe_record_count = 0
    for count, record in records.items():
        logging.debug(f"current record: {record}")
        if not is_safe(record):
            continue
        safe_record_count += 1
    logging.info(f"Answer: {safe_record_count}")


if __name__ == "__main__":
    main()