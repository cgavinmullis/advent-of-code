import argparse
import logging

# Configure logging to output to both console and a file
logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[logging.FileHandler("output.log"), logging.StreamHandler()],
)

INCREASING = "increasing"
DECREASING = "decreasing"
FLAT = "flat"

def get_difference(num1, num2):
    decreasing = False
    increasing = False
    difference = int(num1) - int(num2)
    if difference > 0:
        return DECREASING
    elif difference < 0:
        return INCREASING
    return {"increasing": increasing, "decreasing": decreasing}

def is_safe(record):

    slope = None
    prev_slope = None
    for index, level in enumerate(record):
        if index == 0:
            continue
        left = record[index - 1]
        right = level
        slope = get_difference(left, right)
        # Check left of cursor and cursor
        if not are_levels_safe(left, right, slope, prev_slope):
            return False
        prev_slope = slope
        

    logging.debug("Safe")
    return True

def are_levels_safe(left, right, slope, prev_slope):
    logging.debug(f"{left} {right} {slope} {prev_slope}")
    if left == right:
        # numbers are equal, record is unsafe
        logging.debug(f"Unsafe equal nums: {left} and {right}")
        return False
    if slope != prev_slope and (prev_slope is not None):
        # the slope has changed directions
        logging.debug("Unsafe changed slope")
        return False
    if not are_adjacent_levels_safe(left, right):
        logging.debug(f"Unsafe adjacent distance: {left} and {right}")
        return False
    return True
def are_adjacent_levels_safe(left, right):
    distance = abs(int(left) - int(right))

    if distance > 3 or distance < 1:
        return False
    
    return True

def process_input(raw_input):
    records = {}
    for index, line in raw_input:
        line = line.rstrip()
        logging.debug(f"reading line: {line}")
        records[index] = line.split()

    logging.debug(f"records: {records}")
    return records

def main():
    # Set up command line argument parsing
    parser = argparse.ArgumentParser(description="Process lines from an input file.")
    parser.add_argument("input_file", type=str, help="Path to the input file")

    args = parser.parse_args()

    logging.info(f"Reading lines from file: {args.input_file}")

    records = {}
    try:
        # Open the input file
        with open(args.input_file, "r") as file:
            # Read and process each line
            records = process_input(enumerate(file))
    except FileNotFoundError:
        logging.error(f"File not found: {args.input_file}")
    logging.info("")

    # Do work here
    safe_record_count = 0
    for count, record in records.items():
        safe_perm = True
        logging.debug(f"current record: {record}")
        if not is_safe(record):
            safe_perm = False
            for index, item in enumerate(record):
                logging.debug(f"current perm: {record[:index] + record[index+1:]}")
                if is_safe(record[:index] + record[index+1:]):
                    safe_perm = True
                    break
            if not safe_perm:
                logging.debug(f"\nunsafe record {record}\n")
                continue
        safe_record_count += 1
    logging.info(f"Answer: {safe_record_count}")


if __name__ == "__main__":
    main()