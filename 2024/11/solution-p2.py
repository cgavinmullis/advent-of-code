import argparse
import logging

import functools

# Configure logging to output to both console and a file
logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[logging.FileHandler("output.log"), logging.StreamHandler()],
)

def process_input(raw_input):
    for index, line in raw_input:
        line = line.rstrip()
        logging.debug(f"reading line: {line}")
        return tuple(map(int,line.split(" ")))


@functools.cache
def do_blinks(stone, blinks_left):
    logging.debug(f"stone: {stone}, blinks: {blinks_left}")
    if blinks_left == 0:
        return 1
    stones = blink(stone)
    return sum(do_blinks(new_stone, blinks_left-1) for new_stone in stones)

def blink(stone):
    STONE_MULTI = 2024
    if stone == 0:
        return [1]
    if len(str(stone)) % 2 == 0:
        string = str(stone)
        first_part, second_part = string[:len(string)//2], string[len(string)//2:]
        return [int(first_part), int(second_part)]
    return [stone*STONE_MULTI]

def main():
    # Set up command line argument parsing
    parser = argparse.ArgumentParser(description="Process lines from an input file.")
    parser.add_argument("input_file", type=str, help="Path to the input file")

    args = parser.parse_args()

    logging.info(f"Reading lines from file: {args.input_file}")

    lines = []
    try:
        # Open the input file
        with open(args.input_file, "r") as file:
            # Read and process each line
            stones = process_input(enumerate(file))
    except FileNotFoundError:
        logging.error(f"File not found: {args.input_file}")
    logging.info("")

    # Do work here
    logging.debug(f"{stones}")
    total = sum(do_blinks(stone, 75) for stone in stones)
    logging.info(f"Answer: {total}")


if __name__ == "__main__":
    main()