import argparse
import logging

# Configure logging to output to both console and a file
logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[logging.FileHandler("output.log"), logging.StreamHandler()],
)

def process_input(raw_input):
    stones = []
    for index, line in raw_input:
        line = line.rstrip()
        logging.debug(f"reading line: {line}")
        nums = line.split(" ")
        for num in nums:
            stones.append(int(num))
    return stones

def blink(stones):
    STONE_MULTI = 2024
    new_stones = []
    for stone in stones:
        if stone == 0:
            new_stones.append(1)
        elif len(str(stone)) % 2 == 0:
            string = str(stone)
            first_part, second_part = string[:len(string)//2], string[len(string)//2:]
            new_stones.append(int(first_part))
            new_stones.append(int(second_part))
        else:
            new_stones.append(stone*STONE_MULTI)
    return new_stones

def main():
    # Set up command line argument parsing
    parser = argparse.ArgumentParser(description="Process lines from an input file.")
    parser.add_argument("input_file", type=str, help="Path to the input file")

    args = parser.parse_args()

    logging.info(f"Reading lines from file: {args.input_file}")

    lines = []
    try:
        # Open the input file
        with open(args.input_file, "r") as file:
            # Read and process each line
            stones = process_input(enumerate(file))
    except FileNotFoundError:
        logging.error(f"File not found: {args.input_file}")
    logging.info("")

    # Do work here
    logging.debug(f"{stones}")
    for i in range(7):
        logging.debug(f"i: {i}")
        stones = blink(stones)
        #logging.debug(f"{stones}")

    logging.info(f"Answer: {len(stones)}")


if __name__ == "__main__":
    main()