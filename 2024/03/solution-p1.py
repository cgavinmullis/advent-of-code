import argparse
import logging

# Configure logging to output to both console and a file
logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[logging.FileHandler("output.log"), logging.StreamHandler()],
)

def process_input(raw_input):
    lines = []
    for index, line in raw_input:
        line = line.rstrip()
        logging.debug(f"reading line: {line}")
        lines.append(line)
    return lines

def main():
    # Set up command line argument parsing
    parser = argparse.ArgumentParser(description="Process lines from an input file.")
    parser.add_argument("input_file", type=str, help="Path to the input file")

    args = parser.parse_args()

    logging.info(f"Reading lines from file: {args.input_file}")

    lines = []
    try:
        # Open the input file
        with open(args.input_file, "r") as file:
            # Read and process each line
            lines = process_input(enumerate(file))
    except FileNotFoundError:
        logging.error(f"File not found: {args.input_file}")
    logging.info("")

    # Do work here
    parsing_char = False
    looking_for = "m"
    looking_for_do = "d"
    looking_for_dont = "d"
    running_first_digit = ""
    running_second_digit = ""
    total = 0
    enabled = True
    for line in lines:
        for char in line:
            if char == "m" and looking_for == "m":
                looking_for = "u"
                continue
            if char == "u" and looking_for == "u":
                looking_for = "l"
                continue
            if char == "l" and looking_for == "l":
                looking_for = "("
                continue
            if char == "(" and looking_for == "(":
                looking_for = "first digit"
                running_first_digit = ""
                continue
            if looking_for == "first digit" and char in ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]:
                running_first_digit += char
                continue
            if looking_for == "first digit" and char == ",":
                running_second_digit = ""
                looking_for = "second digit"
                continue
            if looking_for == "second digit" and char in ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]:
                running_second_digit += char
                continue
            if looking_for == "second digit" and char == ")":
                logging.debug(f"valid mul found {running_first_digit} * {running_second_digit}")
                if enabled:
                    mul = int(running_first_digit) * int(running_second_digit)
                    total += mul
                else:
                    logging.debug("ignoring valid mul as disabled")
                looking_for = "m"
                continue
            if looking_for_do == "d" and char == "d":
                looking_for_do = "o"
                continue
            if looking_for_do == "o" and char == "o":
                looking_for_do = "("
                looking_for_dont = "n"
                continue
            if looking_for_do == "(" and char == "(":
                looking_for_do = ")"
                looking_for_dont = "d"
                continue
            if looking_for_do == ")" and char == ")":
                logging.debug("enabled")
                enabled = True
                looking_for_do = "d"
                looking_for_dont = "d"
                continue
            if looking_for_dont == "n" and char == "n":
                looking_for_do = "d"
                looking_for_dont = "'"
                continue
            if looking_for_dont == "'" and char == "'":
                looking_for_do = "d"
                looking_for_dont = "t"
                continue
            if looking_for_dont == "t" and char == "t":
                looking_for_do = "d"
                looking_for_dont = "("
                continue
            if looking_for_dont == "(" and char == "(":
                looking_for_dont = ")"
                looking_for_do = "d"
                continue
            if looking_for_dont == ")" and char == ")":
                enabled = False
                logging.debug("disabled")
                looking_for_do = "d"
                looking_for_dont = "d"
                continue
            looking_for = "m"
            looking_for_do = "d"
            looking_for_dont = "d"

    logging.info(f"Answer: {total}")


if __name__ == "__main__":
    main()