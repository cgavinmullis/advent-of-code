import argparse
import logging

# Configure logging to output to both console and a file
logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[logging.FileHandler("output.log"), logging.StreamHandler()],
)

def process_input(raw_input):
    lines = []
    for index, line in raw_input:
        line = line.rstrip()
        logging.debug(f"reading line: {line}")
        lines.append(line)
    return lines

def main():
    # Set up command line argument parsing
    parser = argparse.ArgumentParser(description="Process lines from an input file.")
    parser.add_argument("input_file", type=str, help="Path to the input file")

    args = parser.parse_args()

    logging.info(f"Reading lines from file: {args.input_file}")

    lines = []
    try:
        # Open the input file
        with open(args.input_file, "r") as file:
            # Read and process each line
            lines = process_input(enumerate(file))
    except FileNotFoundError:
        logging.error(f"File not found: {args.input_file}")
    logging.info("")

    # Do work here

    logging.info(f"Answer: ")


if __name__ == "__main__":
    main()