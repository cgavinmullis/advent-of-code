import argparse
import logging

# Configure logging to output to both console and a file
logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[logging.FileHandler("output.log"), logging.StreamHandler()],
)

X = 0
Y = 1
HEIGHT = 103
WIDTH = 101

EXAMPLE = False
if EXAMPLE:
    HEIGHT = 7
    WIDTH = 11

class Robot:

    def __init__(self, start_x, start_y, velocity_x, velocity_y):
        self.position = [start_x, start_y]
        self.velocity = [velocity_x, velocity_y]

    def move(self, count=1):
        self.position[X] += count*self.velocity[X]
        self.position[Y] += count*self.velocity[Y]
        self._normalize_bounds()

    def _normalize_bounds(self):
        logging.debug(f"Pre-Normalize: p={self.position[X]},{self.position[Y]}")
        self.position[X] = self.position[X] % WIDTH
        self.position[Y] = self.position[Y] % HEIGHT
        logging.debug(f"PostNormalize: p={self.position[X]},{self.position[Y]}")

    def get_position(self) -> list:
        return self.position
    
    def get_velocity(self) -> list:
        return self.velocity
    
class QuadrantCounter:
    QUAD_MAP = {
        "TOP_LEFT": 0,
        "TOP_RIGHT": 1,
        "BOT_LEFT": 2,
        "BOT_RIGHT": 3,
        "NONE": 4,
    }

    def __init__(self):
        self.quadrant_vertical_center = WIDTH // 2
        self.quadrant_horizontal_center = HEIGHT // 2
        self.visited = {0: [], 1: [], 2: [], 3: [], 4: []}
        self.quadrant_counts = {0: 0, 1: 0, 2: 0, 3: 0}

    def determine_quadrant(self, position):
        if position[X] == self.quadrant_vertical_center or position[Y] == self.quadrant_horizontal_center:
            return self.QUAD_MAP["NONE"]
        if position[X] < self.quadrant_vertical_center:
            if position[Y] < self.quadrant_horizontal_center:
                return self.QUAD_MAP["TOP_LEFT"]
            else:
                return self.QUAD_MAP["TOP_RIGHT"]
            
        if position[X] > self.quadrant_vertical_center:
            if position[Y] < self.quadrant_horizontal_center:
                return self.QUAD_MAP["BOT_LEFT"]
            else:
                return self.QUAD_MAP["BOT_RIGHT"]
        return self.QUAD_MAP["NONE"]
    
    def determine_quadrant_count(self, positions):

        for key, position in positions.items():
            quad = self.determine_quadrant(position)
            logging.info(f"position: {position} in quad {quad}")
            if quad == self.QUAD_MAP["NONE"]:
                logging.debug(f"robot: {key} found at {position} in no QUAD")
                continue
            logging.debug(f"p={position}")
            logging.debug(f"visited {[quad]}: {self.visited[quad]}")
            if position in self.visited[quad]:
                logging.debug(f"robot {key} dup found at {position} same as {self.visited[quad].index(position)}")
            else:
                self.visited[quad].append(position)
            self.quadrant_counts[quad] += 1

        return self.quadrant_counts

def determine_safety_factor(robot_counts):
    safety_factor = 1
    for _, count in robot_counts.items():
        logging.debug(f"safety_factor: {safety_factor} * {count}")
        safety_factor *= count

    return safety_factor

def process_input(raw_input):
    robots = []
    for index, line in raw_input:
        line = line.rstrip()
        logging.debug(f"reading line: {line}")

        positions = line.split(" ")[0].split("=")[1].split(",")
        position_x = int(positions[X])
        position_y = int(positions[Y])

        velocities = line.split(" ")[1].split("=")[1].split(",")
        velocity_x = int(velocities[X])
        velocity_y = int(velocities[Y])

        robots.append(Robot(position_x, position_y, velocity_x, velocity_y))
    return robots

def pretty_print_robots(robots, split=False):
    final_output = []
    for i in range(HEIGHT):
        final_output.append([])
        for j in range(WIDTH):
            final_output[i].append(0)

    logging.debug(final_output)
    for robot in robots:
        position = robot.get_position()
        logging.debug(f"pos={position}")
        logging.debug(f"{final_output[position[Y]][position[X]]}")
        final_output[position[Y]][position[X]] = final_output[position[Y]][position[X]] + 1
    #logging.info("")
    lines = []
    return_val = False
    for index, row in enumerate(final_output):
        line = ""
        for index_col, item in enumerate(row):
            if item == 0:
                line += "."
            else:
                line += "#"
        lines.append(line)
        if "#########" in line:
            return_val = True
    
    if return_val:
        for line in lines:
            logging.info(f"{line}")
    return return_val
def main():
    # Set up command line argument parsing
    parser = argparse.ArgumentParser(description="Process lines from an input file.")
    parser.add_argument("input_file", type=str, help="Path to the input file")

    args = parser.parse_args()

    logging.info(f"Reading lines from file: {args.input_file}")

    robots = []
    try:
        # Open the input file
        with open(args.input_file, "r") as file:
            # Read and process each line
            robots = process_input(enumerate(file))
    except FileNotFoundError:
        logging.error(f"File not found: {args.input_file}")
    logging.info("")
    pretty_print_robots(robots)

    for robot in robots:
        logging.debug(f"p={robot.get_position()}v={robot.get_velocity()}")
    # Do work here
    robot_positions = {}
    frame_count = 1000000
    for frame in range(frame_count):
        count = 0
        for robot in robots:
            robot.move()
            position = robot.get_position()
            logging.debug(f"count: {count} p={position}")
            count += 1
            robot_positions[count] = position
        found = pretty_print_robots(robots)
        if found:
            logging.info(f"Answer: {frame+1}")
            break


if __name__ == "__main__":
    main()