import argparse
import logging
from queue import Queue

# Configure logging to output to both console and a file
logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[logging.FileHandler("output.log"), logging.StreamHandler()],
)

def process_input(raw_input):
    crops = {}
    count = 0
    row_count = 0
    col_count = 0
    for row_index, line in raw_input:
        line = line.rstrip()
        logging.debug(f"reading line: {line}")
        col_count = len(line)
        row_count += 1
        for col_index, char in enumerate(line):
            crops[count] = {
                "char": char,
                "row_index": row_index,
                "col_index": col_index,
            }
            count +=1
    return crops, row_count, col_count

def calculate_region_price(area, perimeter):
    return area * perimeter

def get_key_from_value(dictionary, row, col):
    for key, value in dictionary.items():
        if value["row_index"] == row and value["col_index"] == col:
            return key 


def flood_fill_search(crops, start_key, row_count, col_count, visited) -> dict:
    area = 0
    perimeter = 0

    row_index = crops[start_key].get("row_index")
    col_index = crops[start_key].get("col_index")
    starting_crop = crops[start_key].get("char")
    if start_key in visited:
        return {"area": area, "perimeter": perimeter, "visited": visited}
    queue = Queue()
    queue.put((row_index, col_index, start_key))
    local_visited = []
    while not queue.empty():
        row_index, col_index, key = queue.get()
        logging.debug(f"popped queue: [{row_index},{col_index}] [{key},{crops.get(key, {}).get("char", None)}]")
        if key in local_visited:
            continue
        if row_index < 0 or row_index >= row_count or col_index < 0 or col_index >= col_count or crops.get(key, {}).get("char", None) != starting_crop:
            # We must have hit an edge
            perimeter += 1
            logging.debug(f"hit edge at {key}: {row_index},{col_index}")
            continue
        else:
            queue.put((row_index+1, col_index, get_key_from_value(crops, row_index+1, col_index)))
            queue.put((row_index-1, col_index, get_key_from_value(crops, row_index-1, col_index)))
            queue.put((row_index, col_index+1, get_key_from_value(crops, row_index, col_index+1)))
            queue.put((row_index, col_index-1, get_key_from_value(crops, row_index, col_index-1)))
        area += 1
        local_visited.append(key)
        visited.append(key)

    return {"area": area, "perimeter": perimeter, "visited":visited}


def main():
    # Set up command line argument parsing
    parser = argparse.ArgumentParser(description="Process lines from an input file.")
    parser.add_argument("input_file", type=str, help="Path to the input file")

    args = parser.parse_args()

    logging.info(f"Reading lines from file: {args.input_file}")

    crops = {}
    row_count = 0
    col_count = 0
    try:
        # Open the input file
        with open(args.input_file, "r") as file:
            # Read and process each line
            crops, row_count, col_count = process_input(enumerate(file))
    except FileNotFoundError:
        logging.error(f"File not found: {args.input_file}")
    logging.info("")

    logging.debug(f"row_count: {row_count}")
    logging.debug(f"col_count: {col_count}")
    logging.debug(f"crops: {crops}")
    # Do work here
    visited = []
    running_sum = 0
    for crop in crops:
        if crop in visited:
            continue
        logging.info(f"checking crop: {crops.get(crop, {}).get("char", None)}, key:{crop}")
        flooded = flood_fill_search(crops, crop, row_count, col_count, visited)
        visited = flooded["visited"]
        region_price = calculate_region_price(flooded["area"], flooded["perimeter"])
        logging.debug(f"region of {crops[crop]["char"]} has area {flooded["area"]} and perimeter {flooded["perimeter"]}")
        logging.debug(f"region price is {region_price}")
        running_sum += region_price
        
    logging.info(f"Answer: {running_sum}")


if __name__ == "__main__":
    main()