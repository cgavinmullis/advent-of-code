import argparse
import logging

# Configure logging to output to both console and a file
logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[logging.FileHandler("output.log"), logging.StreamHandler()],
)

def pretty_print_list(compressed_format):
    line = ""
    for val in compressed_format:
        line += str(val)
    logging.debug(f"{line}")

def process_input(raw_input):
    dense_format = []
    for index, line in raw_input:
        line = line.rstrip()
        logging.debug(f"reading line: {line}")
        for char in line:
            dense_format.append(int(char))
    return dense_format

def explode_dense_format(disk_map):
    final_output = []
    is_file = False
    was_file = False
    current_data_length = 0
    current_space_length = 0
    current_file_id = -1
    free_space_dict = {}
    free_space_priority = []
    file_id_dict = {}
    for index, number in enumerate(disk_map):
        if index % 2 == 0:
            # Num is even
            is_file = True
            was_file = True
            current_file_id += 1
        else:
            is_file = False
            # This is free space
        current_data_length = 0
        if not is_file:
            free_space_priority.append([number, len(final_output), len(final_output) + number - 1])
        for i in range(number):
            if is_file:
                final_output.append(str(current_file_id))
            else:
                final_output.append(".")
            current_data_length += 1
        if is_file:
            file_id_dict[current_file_id] = [index, index + current_data_length - 1]

    return final_output, free_space_dict, file_id_dict, free_space_priority


def get_needed_free_space(file_list):
    START_INDEX = 0
    STOP_INDEX = 1
    return file_list[STOP_INDEX] - file_list[START_INDEX] + 1

def compress_exploded_white_house(exploded_format, free_space_dict, file_id_dict, free_space_priority):
    START_INDEX = 0
    STOP_INDEX = 1
    right_cursor = len(exploded_format)-1
    logging.debug(f"files to sort: {file_id_dict}")
    while right_cursor >= 0:
        logging.debug(f"right_cursor: {right_cursor}")
        file_id = exploded_format[right_cursor]
        if file_id == ".":
            right_cursor -= 1
            continue
        file_id = int(file_id)
        needed_free_space = get_needed_free_space(file_id_dict[file_id])
        logging.debug(f"file_id: {file_id}, needed_free_space: {needed_free_space}")
        logging.debug(f"free_space_prio: {free_space_priority}")

        for free_space in free_space_priority:
            if free_space[0] >= needed_free_space:
                if free_space[1] >= right_cursor:
                    continue
                logging.debug(f"free_space_prio: {free_space_priority}")
                length = free_space[0]
                start = free_space[1]
                stop = start + needed_free_space
                for i in range(needed_free_space):
                    exploded_format[start+i] = str(file_id)
                    exploded_format[right_cursor-i] = "."
                if length > needed_free_space:
                    new_free_space = [length - needed_free_space, start + needed_free_space, stop]
                    logging.debug(f"updated free space from {free_space} to {new_free_space}")
                    free_space_priority[free_space_priority.index(free_space)] = new_free_space
                else:
                    free_space_priority.remove(free_space)
                pretty_print_list(exploded_format)
                break

        right_cursor -= needed_free_space
    return exploded_format

def calc_checksum(compressed_format):
    checksum = 0
    for index, value in enumerate(compressed_format):
        if value == ".":
            continue
        checksum += int(str(value)) * index
    return checksum

def main():
    # Set up command line argument parsing
    parser = argparse.ArgumentParser(description="Process lines from an input file.")
    parser.add_argument("input_file", type=str, help="Path to the input file")

    args = parser.parse_args()

    logging.info(f"Reading lines from file: {args.input_file}")

    dense_format = []
    try:
        # Open the input file
        with open(args.input_file, "r") as file:
            # Read and process each line
            dense_format = process_input(enumerate(file))
    except FileNotFoundError:
        logging.error(f"File not found: {args.input_file}")
    logging.info("")

    # Do work here
    exploded_format, free_space_dict, file_id_dict, free_space_priority = explode_dense_format(dense_format)
    logging.debug(f"{exploded_format}")
    logging.debug(f"free: {free_space_dict}")
    logging.debug(f"files: {file_id_dict}")
    pretty_print_list(exploded_format)
    compressed_format = compress_exploded_white_house(exploded_format, free_space_dict, file_id_dict, free_space_priority)
    pretty_print_list(compressed_format)
    logging.info(f"Answer: {calc_checksum(compressed_format)}")


if __name__ == "__main__":
    main()