import argparse
import logging

# Configure logging to output to both console and a file
logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[logging.FileHandler("output.log"), logging.StreamHandler()],
)

def process_input(raw_input):
    dense_format = []
    for index, line in raw_input:
        line = line.rstrip()
        logging.debug(f"reading line: {line}")
        for char in line:
            dense_format.append(int(char))
    return dense_format

def explode_dense_format(disk_map):
    final_output = []
    is_file = False
    current_file_id = -1
    for index, number in enumerate(disk_map):
        if index % 2 == 0:
            # Num is even
            is_file = True
            current_file_id += 1
        else:
            is_file = False
            # This is free space
        for i in range(number):
            if is_file:
                final_output.append(str(current_file_id))
            else:
                final_output.append(".")
    return final_output


def pretty_print_list(compressed_format):
    line = ""
    for val in compressed_format:
        line += str(val)
    logging.debug(f"{line}")

def compress_exploded_white_house(exploded_format):
    left_cursor = 0
    right_cursor = len(exploded_format)-1
    while left_cursor != right_cursor:
        if exploded_format[right_cursor] == ".":
            right_cursor -= 1
            continue
        if exploded_format[left_cursor] != ".":
            left_cursor += 1
            continue
        exploded_format[left_cursor], exploded_format[right_cursor] = exploded_format[right_cursor], exploded_format[left_cursor]
        #pretty_print_list(exploded_format)
    return exploded_format

def calc_checksum(compressed_format):
    checksum = 0
    for index, value in enumerate(compressed_format):
        if value == ".":
            continue
        checksum += int(value) * index
    return checksum

def main():
    # Set up command line argument parsing
    parser = argparse.ArgumentParser(description="Process lines from an input file.")
    parser.add_argument("input_file", type=str, help="Path to the input file")

    args = parser.parse_args()

    logging.info(f"Reading lines from file: {args.input_file}")

    dense_format = []
    try:
        # Open the input file
        with open(args.input_file, "r") as file:
            # Read and process each line
            dense_format = process_input(enumerate(file))
    except FileNotFoundError:
        logging.error(f"File not found: {args.input_file}")
    logging.info("")

    # Do work here
    exploded_format = explode_dense_format(dense_format)
    logging.debug(f"{exploded_format}")
    pretty_print_list(exploded_format)
    compressed_format = compress_exploded_white_house(exploded_format)
    pretty_print_list(compressed_format)
    logging.info(f"Answer: {calc_checksum(compressed_format)}")


if __name__ == "__main__":
    main()