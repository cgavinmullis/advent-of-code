import argparse
import logging

# Configure logging to output to both console and a file
logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[logging.FileHandler("output.log"), logging.StreamHandler()],
)

def find_distance(num1, num2):
    return abs(num1 - num2)

def main():
    # Set up command line argument parsing
    parser = argparse.ArgumentParser(description="Process lines from an input file.")
    parser.add_argument("input_file", type=str, help="Path to the input file")

    args = parser.parse_args()

    logging.info(f"Reading lines from file: {args.input_file}")

    lines = []
    left = []
    right = []
    distances = []
    total = 0
    try:
        # Open the input file
        with open(args.input_file, "r") as file:
            # Read and process each line
            for index, line in enumerate(file):
                line = line.rstrip()
                logging.debug(f"reading line: {line}")
                lines.append(line)
                left.append(int(line.split()[0]))
                right.append(int(line.split()[-1]))
    except FileNotFoundError:
        logging.error(f"File not found: {args.input_file}")
    logging.info("")
    logging.debug(f"left and right:\n{left}\n{right}")
    left.sort()
    right.sort()
    logging.debug(f"left and right sorted:\n{left}\n{right}")
    for index, (item1, item2) in enumerate(zip(left, right)):
        distances.append(find_distance(item1, item2))
        total += distances[-1]
        logging.debug(f"running total:{total}")
    logging.info(f"Answer: {total}")


if __name__ == "__main__":
    main()