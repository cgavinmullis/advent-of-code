import argparse
import logging

# Configure logging to output to both console and a file
logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[logging.FileHandler("output.log"), logging.StreamHandler()],
)

def process_input(raw_input):
    antenna_dict = {}
    max_col = 0
    max_row = 0
    for index, line in raw_input:
        line = line.rstrip()
        logging.debug(f"reading line: {line}")
        max_col = len(line) - 1
        for col_index, char in enumerate(line):
            if char == ".":
                continue
            if char not in antenna_dict.keys():
                antenna_dict[char] = [[index, col_index]]
            else:
                antenna_dict[char].append([index, col_index])
        max_row += 1
    return antenna_dict, max_row-1, max_col

def find_vector_between_two_nodes(node1, node2):
    return [node1[0] - node2[0], node1[1] - node2[1]]

def pretty_print_antinodes(max_row, max_col, antinodes):
    logging.debug(f"{max_row}, {max_col}: {antinodes}")
    antinode_count = 0
    for i in range(max_row+1):
        line = ""
        for j in range(max_col+1):
            if [i, j] in antinodes:
                line += "#"
                antinode_count += 1
            else:
                line += "."
        logging.debug(f"{line}")
    return antinode_count

def main():
    # Set up command line argument parsing
    parser = argparse.ArgumentParser(description="Process lines from an input file.")
    parser.add_argument("input_file", type=str, help="Path to the input file")

    args = parser.parse_args()

    logging.info(f"Reading lines from file: {args.input_file}")

    antenna_dict = {}
    try:
        # Open the input file
        with open(args.input_file, "r") as file:
            # Read and process each line
            antenna_dict, max_row, max_col = process_input(enumerate(file))
            logging.debug(f"Max Row: {max_row}")
            logging.debug(f"Max Col: {max_col}")
    except FileNotFoundError:
        logging.error(f"File not found: {args.input_file}")
    logging.info("")

    # Do work here
    
    logging.debug(f"{antenna_dict}")
    found_antinodes = []
    for key, antennas in antenna_dict.items():
        for primary_node in antennas:
            logging.debug(f"{primary_node}")
            for second_node in antenna_dict[key]:
                if second_node == primary_node:
                    continue
                vector = find_vector_between_two_nodes(primary_node, second_node)
                logging.debug(f"{key} vector between {primary_node} and {second_node}: {vector}")
                antinode = [x + y for x, y in zip(primary_node, vector)]
                if antinode[0] < 0 or antinode[1] < 0:
                    continue
                if antinode[0] > max_row or antinode[1] > max_col:
                    continue
                found_antinodes.append(antinode)
                logging.info(f"Found valid antinode: {antinode}")
    antinode_count = pretty_print_antinodes(max_row, max_col, found_antinodes)
    logging.info(f"Answer: {antinode_count}")


if __name__ == "__main__":
    main()