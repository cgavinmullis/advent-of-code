import argparse
import logging

# Configure logging to output to both console and a file
logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[logging.FileHandler("output.log"), logging.StreamHandler()],
)

ROW = 0
COLUMN = 1

def process_input(raw_input):
    board = []
    starting_position = [0,0]
    for row_index, line in raw_input:
        line = line.rstrip()
        logging.debug(f"reading line: {line}")
        row = []
        for col_index, char in enumerate(line):
            if char == "^":
                starting_position = [row_index, col_index]
                char = 0
            if char == ".":
                char = 0
            else:
                char = -1
            row.append(char)
        board.append(row)
    return board, starting_position

def turn(current_direction):
    new_direction = (current_direction + 1 % 4)
    return new_direction if new_direction < 4 else 0

def step(current_position, current_direction):
    next_step_map = {
        0: [-1, 0],
        3: [0, -1],
        2: [1, 0],
        1: [0,1]
    }
    return [row + col for row,col in zip(current_position, next_step_map[current_direction])]

def next_step_off_board(current_position, current_direction, board):
    next_position = step(current_position, current_direction)
    max_row = len(board[ROW]) - 1
    max_col = len(board) - 1

    if next_position[ROW] < 0 or next_position[ROW] > max_row:
        return True
    if next_position[COLUMN] < 0 or next_position[COLUMN] > max_col:
        return True
    return False

def object_in_front(current_position, current_direction, board):
    next_position = step(current_position, current_direction)
    logging.debug(f"current_position: {current_position}")
    logging.debug(f"current_direction:{current_direction}")
    logging.debug(f"next_position: {next_position}")
    logging.debug(f"boar_position: {board[next_position[ROW]][next_position[COLUMN]]}")
    if board[next_position[ROW]][next_position[COLUMN]] < 0:

        return True
    return False
        
def visit_space(current_position, board):
    board[current_position[ROW]][current_position[COLUMN]] += 1
    return board

def run_simulation(board, current_position):
    direction_map = {0:"UP", 1:"RIGHT", 2:"DOWN", 3:"LEFT"}
    current_direction = 0
    while not next_step_off_board(current_position, current_direction, board):
        board = visit_space(current_position, board)
        pretty_print_board(board)
        if object_in_front(current_position, current_direction, board):
            current_direction = turn(current_direction)
            logging.debug(f"new direction: {direction_map[current_direction]}")
        current_position = step(current_position, current_direction)
    board = visit_space(current_position, board)
    return board

def count_visits(board):
    visits = 0
    for row in board:
        for space in row:
            if space > 0:
                visits += 1
    return visits

def pretty_print_board(board):
    logging.info("\n")
    for row in board:
        pretty_row = ""
        for char in row:
            if char > 0:
                pretty_row += "X"
            if char == 0:
                pretty_row += "."
            if char < 0:
                pretty_row += "#"
        logging.info(f"{pretty_row}")

def main():
    # Set up command line argument parsing
    parser = argparse.ArgumentParser(description="Process lines from an input file.")
    parser.add_argument("input_file", type=str, help="Path to the input file")

    args = parser.parse_args()

    logging.info(f"Reading lines from file: {args.input_file}")

    try:
        # Open the input file
        with open(args.input_file, "r") as file:
            # Read and process each line
            board, starting_position = process_input(enumerate(file))
    except FileNotFoundError:
        logging.error(f"File not found: {args.input_file}")
    logging.info("")

    # Do work here
    board = run_simulation(board, starting_position)
    total_unique_visits = count_visits(board)
    pretty_print_board(board)
    logging.info(f"Answer: {total_unique_visits}")


if __name__ == "__main__":
    main()