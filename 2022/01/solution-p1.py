
most_calories = 0
current_calories = 0



with open("input") as input:
    for line in input:
        line = line.rstrip()
        print(line)
        if not line:
            print("Blank Line indicates end of elfs inventory")
            if current_calories > most_calories:
                print(f"New highest count found {current_calories}")
                most_calories = current_calories
            current_calories = 0
        else:
            current_calories = current_calories + int(line)

print(f"The elf carrying the most calories is carrying {most_calories}!")
