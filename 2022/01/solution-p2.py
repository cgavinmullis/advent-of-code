
most_calories = 0
current_calories = 0
top_3 = [0,0,0]

def replace_top_3(top_3, current):
    for i, index in enumerate(top_3):
        if current > i:
            top_3.insert(index, current)
            print("new total")
            print(sum(top_3[:2]))

with open("input") as input:
    for line in input:
        line = line.rstrip()
        print(line)
        if not line:
            print("Blank Line indicates end of elfs inventory")
            if current_calories > top_3[2]:
                print(f"New highest count found {current_calories}")
                replace_top_3(top_3, current_calories)
            current_calories = 0
        else:
            current_calories = current_calories + int(line)
