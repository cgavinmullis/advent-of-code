import argparse
import logging

from itertools import cycle
from math import gcd

# Configure logging to output to both console and a file
logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[logging.FileHandler("output.log"), logging.StreamHandler()],
)

TURN = {
    "L": 0,
    "R": 1,
}

class Navigator:

    def __init__(self, instructions, raw_network):
        self.raw_instructions = instructions
        self.parsed_instructions = self.parse_instructions()
        logging.info(f"parsed_instructions: {self.parsed_instructions}")
        self.raw_network = raw_network
        self.starting_nodes = []
        self.ending_nodes = []
        self.parsed_network = self.parse_network()
        logging.info(f"starting_nodes: {self.starting_nodes}")
        logging.info(f"ending_nodes: {self.ending_nodes}")
        logging.debug(f"parsed_network: {self.parsed_network}")
        self.steps = 0
        self.step_solutions = []

    def parse_instructions(self):
        return list(self.raw_instructions)

    def parse_network(self):
        network = {}
        self.starting_nodes = []
        self.ending_nodes = []
        for line in self.raw_network:
            node = line.split()[0]
            logging.debug(f"parsing node: {node}")
            left_connected_edge = line.split("(")[1].split(",")[0]
            logging.debug(f"left_connected_edge: {left_connected_edge}")
            right_connected_edge = line.split(",")[1].split()[0][:-1]
            logging.debug(f"right_connected_edge: {right_connected_edge}")
            network[node] = [left_connected_edge, right_connected_edge]
            if node[-1] == "A":
                self.starting_nodes.append( {node: network[node]})
            if node[-1] == "Z":
                self.ending_nodes.append({node: network[node]})
        return network

    def follow_instructions(self):
        
        self.step_solutions
        for index, node in enumerate(self.starting_nodes):
            steps = 0
            instruction_cycle = cycle(self.parsed_instructions)

            logging.debug(f"current node: {node}")
            current_node = list(node.keys())[0]
            logging.debug(f"current node: {current_node}")
            while current_node[-1] != "Z":
                steps += 1
                step = next(instruction_cycle)
                logging.debug(f"step: {step}")
                logging.debug(f"current node: {current_node}")

                current_node = self.parsed_network[current_node][TURN[step]]
                logging.debug(f"next node: {current_node}")
                node = self.parsed_network[current_node]
                logging.debug(f"new current node: {node}")
                
            self.step_solutions.append(steps)
        logging.info(f"step_solutions: {self.step_solutions}")
        return

    def find_lcm(self):
        a = self.step_solutions[0]
        logging.debug(f"a: {a}")
        for b in self.step_solutions[1:]:
            a = (a * b) // gcd (a, b)
            logging.debug(f"a: {a}")
        return a




def main():
    # Set up command line argument parsing
    parser = argparse.ArgumentParser(description="Process lines from an input file.")
    parser.add_argument("input_file", type=str, help="Path to the input file")

    args = parser.parse_args()

    logging.info(f"Reading lines from file: {args.input_file}")

    lines = []
    instructions = None
    try:
        # Open the input file
        with open(args.input_file, "r") as file:
            # Read and process each line
            for index, line in enumerate(file):
                line = line.rstrip()
                logging.debug(f"reading line: {line}")
                if index == 0:
                    instructions = line
                    continue
                if line:
                    lines.append(line)
    except FileNotFoundError:
        logging.error(f"File not found: {args.input_file}")
    logging.info("")

    # Do work here
    navi = Navigator(instructions, lines)
    navi.follow_instructions()
    logging.info(f"Answer: {navi.find_lcm()}")


if __name__ == "__main__":
    main()