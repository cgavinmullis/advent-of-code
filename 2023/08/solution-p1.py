import argparse
import logging

from itertools import cycle

# Configure logging to output to both console and a file
logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[logging.FileHandler("output.log"), logging.StreamHandler()],
)

TURN = {
    "L": 0,
    "R": 1,
}

class Navigator:

    def __init__(self, instructions, raw_network):
        self.raw_instructions = instructions
        self.parsed_instructions = self.parse_instructions()
        logging.info(f"parsed_instructions: {self.parsed_instructions}")
        self.raw_network = raw_network
        self.parsed_network = self.parse_network()
        logging.debug(f"parsed_network: {self.parsed_network}")
        self.steps = 0

    def parse_instructions(self):
        return list(self.raw_instructions)

    def parse_network(self):
        network = {}
        for line in self.raw_network:
            node = line.split()[0]
            logging.debug(f"parsing node: {node}")
            left_connected_edge = line.split("(")[1].split(",")[0]
            logging.debug(f"left_connected_edge: {left_connected_edge}")
            right_connected_edge = line.split(",")[1].split()[0][:-1]
            logging.debug(f"right_connected_edge: {right_connected_edge}")
            network[node] = [left_connected_edge, right_connected_edge]
        return network

    def follow_instructions(self):
        
        current_node = self.parsed_network["AAA"]
        end_node = self.parsed_network["ZZZ"]

        instruction_cycle = cycle(self.parsed_instructions)

        while current_node != end_node:
            self.steps += 1
            step = next(instruction_cycle)
            logging.debug(f"step: {step}")
            logging.debug(f"current node: {current_node}")
            next_node = current_node[TURN[step]]
            logging.debug(f"next node: {next_node}")
            current_node = self.parsed_network[next_node]
            logging.debug(f"current node: {current_node}")
        return

def main():
    # Set up command line argument parsing
    parser = argparse.ArgumentParser(description="Process lines from an input file.")
    parser.add_argument("input_file", type=str, help="Path to the input file")

    args = parser.parse_args()

    logging.info(f"Reading lines from file: {args.input_file}")

    lines = []
    instructions = None
    try:
        # Open the input file
        with open(args.input_file, "r") as file:
            # Read and process each line
            for index, line in enumerate(file):
                line = line.rstrip()
                logging.debug(f"reading line: {line}")
                if index == 0:
                    instructions = line
                    continue
                if line:
                    lines.append(line)
    except FileNotFoundError:
        logging.error(f"File not found: {args.input_file}")
    logging.info("")

    # Do work here
    navi = Navigator(instructions, lines)
    navi.follow_instructions()
    logging.info(f"Answer: {navi.steps}")


if __name__ == "__main__":
    main()