import argparse
import logging
from math import sqrt, ceil, floor

# Configure logging to output to both console and a file
logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[logging.FileHandler("output.log"), logging.StreamHandler()],
)


class races:
    def __init__(self, time_line, distance_line):
        self.race_times = "".join(time_line.split(":")[1].split())
        logging.info(f"{self.race_times}")
        self.race_record = "".join(distance_line.split(":")[1].split())
        logging.info(f"{self.race_record}")
        self.race_map = self.map_races()
        self.zeros = self.calculate_zeros()
        self.solution = self.calculate_solution()

    def map_races(self):
        race_map = {}
        race_map[int(self.race_times)] = int(self.race_record)
        logging.info(f"{race_map}")
        return race_map

    def calculate_zeros(self):
        zeros = []

        for time, distance in self.race_map.items():
            zero_list = []
            zero_list.append(
                ceil((time - sqrt((time * time) - (4 * distance + 1))) / 2)
            )
            zero_list.append(
                floor((time + sqrt((time * time) - (4 * distance + 1))) / 2)
            )
            zeros.append(zero_list)
            logging.info(f"zeros: {zero_list}")
        return zeros

    def calculate_solution(self):
        solution = 1
        for zeros in self.zeros:
            interval = zeros[1] - zeros[0] + 1
            logging.info(f"interval: {interval}")
            solution = solution * interval
        return solution

    def get_solution(self):
        return self.solution


def main():
    # Set up command line argument parsing
    parser = argparse.ArgumentParser(description="Process lines from an input file.")
    parser.add_argument("input_file", type=str, help="Path to the input file")

    args = parser.parse_args()

    logging.info(f"Reading lines from file: {args.input_file}")

    lines = []
    try:
        # Open the input file
        with open(args.input_file, "r") as file:
            # Read and process each line
            for index, line in enumerate(file):
                line = line.rstrip()
                logging.debug(f"reading line: {line}")
                lines.append(line)
    except FileNotFoundError:
        logging.error(f"File not found: {args.input_file}")
    logging.info("")

    # Do work here
    toy_boats = races(lines[0], lines[1])

    logging.info(f"Answer: {toy_boats.get_solution()}")


if __name__ == "__main__":
    main()
