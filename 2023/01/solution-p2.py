calibration_values = []
english_dict = {
    "one": "1",
    "two": "2",
    "three": "3",
    "four": "4",
    "five": "5",
    "six": "6",
    "seven": "7",
    "eight": "8",
    "nine": "9",
}
 
 
def find_engish_digit_in_line(line):
    for key in english_dict.keys():
        if key in line or key in line[::-1]:
            return english_dict[key]
    return None
 
 
def find_first_digit_in_line(line):
    cumulative_line = ""
    for char in line:
        if char.isdigit():
            return char
        else:
            cumulative_line += char
            result = find_engish_digit_in_line(cumulative_line)
            if result is not None:
                return result
 
 
def find_calibration_values_in_line(line):
    first_digit = find_first_digit_in_line(line)
    second_digit = find_first_digit_in_line(line[::-1])
    return first_digit + second_digit
 
 
with open("input") as input_file:
    for line in input_file:
        line = line.rstrip()
        calibration_values.append(int(find_calibration_values_in_line(line)))
print(sum(calibration_values))
