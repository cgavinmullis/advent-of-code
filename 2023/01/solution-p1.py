calibration_values = []

def find_first_digit_in_line(line):
    for char in line:
        if char.isdigit():
            return char


def find_calibration_values_in_line(line):
    first_digit = find_first_digit_in_line(line)
    second_digit = find_first_digit_in_line(line[::-1])
    return first_digit+second_digit

with open("input") as input_file:
    for line in input_file:
        line = line.rstrip()
        calibration_values.append(int(find_calibration_values_in_line(line)))

print(sum(calibration_values))
