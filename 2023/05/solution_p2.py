import argparse
import logging

# Configure logging to output to both console and a file
logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[logging.FileHandler("output.log"), logging.StreamHandler()],
)


class seed_numbers:
    def __init__(self, line):
        self.seeds = line.split(":")[1].split()
        logging.debug(f"seed list: {self.seeds}")
        self.seed_map = self.gen_seed_dict()
        logging.debug(f"seed map: {self.seed_map}")

    def gen_seed_dict(self):
        seed_map = {}
        for i in range(0, len(self.seeds), 2):
            seed_start = int(self.seeds[i])
            seed_range = int(self.seeds[i + 1])
            logging.debug(f"seed start: {seed_start} to {seed_start+seed_range}")
            seed_map[seed_start] = seed_range
        return seed_map

    def get_seed_map(self):
        return self.seed_map

    def is_seed_valid(self, seed):
        for start_seed, seed_range in self.seed_map.items():
            if seed in range(start_seed, start_seed + seed_range):
                return True
        return False


class x_to_y_map:
    def __init__(self, map_title, map_lines):
        self.source = map_title.split("-")[0]
        self.destination = map_title.split("-")[2].split()[0]
        logging.info(f"Creating {self.source} to {self.destination} map")

        self.map_lines = map_lines
        self.hash_map_list = self.generate_full_map()
        logging.info(f"{self.hash_map_list}")

    def generate_full_map(self):
        hash_map_list = []

        for line in self.map_lines:
            hash_map = {}
            parsed_line = line.split()
            range_val = int(parsed_line[-1])
            source_start = int(parsed_line[1])
            destination_start = int(parsed_line[0])
            logging.debug(
                f"Parsed Line: source:{source_start} destination:{destination_start} range:{range_val}"
            )

            hash_map["source_start"] = source_start
            hash_map["source_end"] = source_start + range_val
            hash_map["destination_start"] = destination_start
            hash_map["destination_end"] = destination_start + range_val

            logging.debug(f"Generated map: {hash_map}")

            hash_map_list.append(hash_map)
        return hash_map_list

    def convert_destination_to_source(self, destination):
        for hash_map in self.hash_map_list:
            if destination in range(
                hash_map["destination_start"], hash_map["destination_end"]
            ):
                source = (destination - hash_map["destination_start"]) + hash_map[
                    "source_start"
                ]
                return source
        return destination


def main():
    # Set up command line argument parsing
    parser = argparse.ArgumentParser(description="Process lines from an input file.")
    parser.add_argument("input_file", type=str, help="Path to the input file")

    args = parser.parse_args()

    logging.info(f"Reading lines from file: {args.input_file}")

    seed_nums = None
    seed_number_map = None
    reading_map = False
    map_title = None
    map_lines = []

    map_list = []

    try:
        # Open the input file
        with open(args.input_file, "r") as file:
            # Read and process each line
            for index, line in enumerate(file):
                line = line.rstrip()
                logging.debug(f"reading line: {line}")

                # Get seed numbers
                if index == 0:
                    seed_nums = seed_numbers(line)
                    seed_number_map = seed_nums.get_seed_map()
                    continue
                # Skip this line :)
                if index == 1:
                    continue
                # Start of map
                if "map" in line:
                    map_title = line
                    continue
                # End of map
                if not line:
                    map_list.append(x_to_y_map(map_title, map_lines))
                    map_title = None
                    map_lines = []
                    continue
                # Reading Map
                map_lines.append(line)
            # End of last map
            map_list.append(x_to_y_map(map_title, map_lines))
    except FileNotFoundError:
        logging.error(f"File not found: {args.input_file}")
    logging.info("")

    # Do work here

    lowest_location = -1
    logging.info(f"seed_number_map: {seed_number_map}")

    for location in range(9999999999):
        logging.info(f"location: {location}")
        for cur_map in reversed(map_list):
            location = cur_map.convert_destination_to_source(location)
            logging.debug(f"{cur_map.source}: {location}")
        seed = location
        logging.debug(f"seed: {seed}")
        if seed_nums.is_seed_valid(seed):
            logging.info(f"FOUND min: {seed}")
            break


if __name__ == "__main__":
    main()
