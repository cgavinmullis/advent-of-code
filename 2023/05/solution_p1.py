import argparse
import logging

# Configure logging to output to both console and a file
logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[logging.FileHandler("output.log"), logging.StreamHandler()],
)


class seed_numbers:
    def __init__(self, line):
        self.seeds = line.split(":")[1].split()
        logging.debug(f"seed list: {self.seeds}")

    def get_seed_numbers(self):
        seed_list = []
        for seed in self.seeds:
            logging.debug(f"seed: {seed}")
            seed_list.append(int(seed))
        return seed_list


class x_to_y_map:
    def __init__(self, map_title, map_lines):
        self.source = map_title.split("-")[0]
        self.destination = map_title.split("-")[2].split()[0]
        logging.info(f"Creating {self.source} to {self.destination} map")

        self.map_lines = map_lines
        self.hash_map_list = self.generate_full_map()
        logging.info(f"{self.hash_map_list}")

    def generate_full_map(self):
        hash_map_list = []

        for line in self.map_lines:
            hash_map = {}
            parsed_line = line.split()
            range_val = int(parsed_line[-1])
            source_start = int(parsed_line[1])
            destination_start = int(parsed_line[0])
            logging.debug(
                f"Parsed Line: source:{source_start} destination:{destination_start} range:{range_val}"
            )

            hash_map["source_start"] = source_start
            hash_map["source_end"] = source_start + range_val
            hash_map["destination_start"] = destination_start

            logging.debug(f"Generated map: {hash_map}")

            hash_map_list.append(hash_map)
        return hash_map_list

    def convert_source_to_destination(self, source):
        for hash_map in self.hash_map_list:
            if source in range(hash_map["source_start"], hash_map["source_end"]):
                destination = (source - hash_map["source_start"]) + hash_map[
                    "destination_start"
                ]
                return destination
        return source


def main():
    # Set up command line argument parsing
    parser = argparse.ArgumentParser(description="Process lines from an input file.")
    parser.add_argument("input_file", type=str, help="Path to the input file")

    args = parser.parse_args()

    logging.info(f"Reading lines from file: {args.input_file}")

    seed_number_list = None
    reading_map = False
    map_title = None
    map_lines = []

    map_list = []

    try:
        # Open the input file
        with open(args.input_file, "r") as file:
            # Read and process each line
            for index, line in enumerate(file):
                line = line.rstrip()
                logging.debug(f"reading line: {line}")

                # Get seed numbers
                if index == 0:
                    seed_number_list = seed_numbers(line).get_seed_numbers()
                    continue
                # Skip this line :)
                if index == 1:
                    continue
                # Start of map
                if "map" in line:
                    map_title = line
                    continue
                # End of map
                if not line:
                    map_list.append(x_to_y_map(map_title, map_lines))
                    map_title = None
                    map_lines = []
                    continue
                # Reading Map
                map_lines.append(line)
            # End of last map
            map_list.append(x_to_y_map(map_title, map_lines))
    except FileNotFoundError:
        logging.error(f"File not found: {args.input_file}")
    logging.info("")

    # Do work here
    location_number_list = []
    for seed in seed_number_list:
        logging.info(f"seed: {seed}")
        for cur_map in map_list:
            seed = cur_map.convert_source_to_destination(seed)
            logging.debug(f"{cur_map.destination}: {seed}")
        location_number_list.append(seed)
        logging.info(f"location: {seed}")
        logging.info("")
    logging.info(f"Lowest location number: {min(location_number_list)}")


if __name__ == "__main__":
    main()
