import argparse
import logging
from collections import Counter

# Configure logging to output to both console and a file
logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[logging.FileHandler("output.log"), logging.StreamHandler()],
)

HAND_TYPES = {
    "Five of a kind": 0,
    "Four of a kind": 1,
    "Full House": 2,
    "Three of a kind": 3,
    "Two Pair": 4,
    "One Pair": 5,
    "High Card": 6,
}

FACE_VALUES = {
    "A": 0,
    "K": 1,
    "Q": 2,
    "T": 4,
    "9": 5,
    "8": 6,
    "7": 7,
    "6": 8,
    "5": 9,
    "4": 10,
    "3": 11,
    "2": 12,
    "J": 13,
}

class Hand():
    def __init__(self, line):
        self.raw_line = line
        self.card_list = list(line.split()[0])
        logging.debug(f"card_list: {self.card_list}")
        self.bet_amount = int(line.split()[1])
        logging.debug(f"bet_amount: {self.bet_amount}")
        self.hand_type = self._determine_hand_type()
        logging.debug(f"hand_type: {self.hand_type}")
        self.hand_value = HAND_TYPES[self.hand_type]
        logging.debug(f"hand_value: {self.hand_value}")
        self.mapped_hand_value = list(map(FACE_VALUES.get, self.card_list))
        logging.debug(f"mapped_hand_value: {self.mapped_hand_value}")

    def _determine_hand_type(self):
        
        hand_type = None
        card_counter = Counter(self.card_list)
        counted = card_counter.most_common(5)
        logging.debug(f"card_counter counted: {counted}")
        for face, count in counted:
            if face == "J":
                logging.debug(f"J found")
                if counted[0][0] != "J":
                    counted[0] = (counted[0][0], counted[0][1] + count)
                elif len(counted) != 1:
                    counted[0] = (counted[1][0], counted[1][1] + count)
                else:
                    return "Five of a kind"
                break
        
        for face, count in counted:
            logging.debug(f"face: {face} count: {count}")
            if face == "J":
                continue
            if count == 5:
                return "Five of a kind"
            elif count == 4:
                return "Four of a kind"
            elif count == 3:
                # Don't think this is possible
                if hand_type == "One Pair":
                    return "Full House"
                else:
                    hand_type = "Three of a kind"
            elif count == 2:
                if hand_type == "Three of a kind":
                    return "Full House"
                elif hand_type == "One Pair":
                    return "Two Pair"
                else:
                    hand_type = "One Pair"
            else:
                if not hand_type:
                    return "High Card"
                return hand_type
        logging.warning(f"Encountered None for hand_type. card_counter: {counted}")
        return "Five of a kind"


class Dealer():
    def __init__(self):
        self.hands = []

    def add_hand(self, hand):
        self.hands.append(hand)

    def custom_sort(self, hand):
        logging.debug(f"{hand.hand_value}{hand.mapped_hand_value}")
        return (hand.hand_value, hand.mapped_hand_value)

    def sort_hands(self):
        self.hands = sorted(self.hands, key=self.custom_sort)

    def get_hands(self):
        for hand in self.hands:
            logging.debug(f"hand: {hand.raw_line}, {hand.hand_type}, {hand.bet_amount}")
        return True

    def calculate_winnings(self):
        total_winnings = 0
        for index, hand in enumerate(reversed(self.hands)):
            total_winnings += hand.bet_amount * (index + 1)
        return total_winnings



def main():
    # Set up command line argument parsing
    parser = argparse.ArgumentParser(description="Process lines from an input file.")
    parser.add_argument("input_file", type=str, help="Path to the input file")

    args = parser.parse_args()

    logging.info(f"Reading lines from file: {args.input_file}")

    lines = []
    dealer = Dealer()
    try:
        # Open the input file
        with open(args.input_file, "r") as file:
            # Read and process each line
            for index, line in enumerate(file):
                line = line.rstrip()
                logging.debug(f"reading line: {line}")
                lines.append(line)

                dealer.add_hand(Hand(line))
    except FileNotFoundError:
        logging.error(f"File not found: {args.input_file}")
    logging.info("")

    # Do work here
    dealer.sort_hands()
    logging.info(f"Answer: {dealer.calculate_winnings()}")


if __name__ == "__main__":
    main()