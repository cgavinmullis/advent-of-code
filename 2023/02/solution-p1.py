

def get_game_id(line):
    return line.split(":")[0].split(" ")[1]

def parse_single_set(game_set):
    set_results = {"red": 0, "green": 0, "blue":0}
    for result in game_set.split(","):
        for color in set_results.keys():
            if color in result:
                set_results[color] = int(result[:-(len(color)+1)])
    print(f"single_set: {set_results}")
    return set_results

def split_game_into_three_sets(game):
    return game.split(";")

def line_is_win(line):
    max_counts = {"red": 12, "green": 13, "blue": 14}
    meta_set = {"red": 0, "green": 0, "blue": 0}
    for game_set in split_game_into_three_sets(line.split(":")[1]):
        parsed_game_set = parse_single_set(game_set)
        for key, value in parsed_game_set.items():
            meta_set[key] += value
            if parsed_game_set[key] > max_counts[key]:
                return False
    print(f"meta_set: {meta_set}")


    return True


cumulative_wins_example = 0
with open("example-p1") as example_p1:
    for line in example_p1:
        if line_is_win(line):
            print(" ")
            cumulative_wins_example += int(get_game_id(line))


cumulative_wins = 0
with open("input-p1") as input_p1:
    for line in input_p1:
        if line_is_win(line):
            print(f"{get_game_id(line)}")
            cumulative_wins += int(get_game_id(line))
print(f"example: {cumulative_wins_example}")
print(f"p1: {cumulative_wins}")
