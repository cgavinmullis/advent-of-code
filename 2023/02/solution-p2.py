
def parse_single_set(game_set):
    set_results = {"red": 0, "green": 0, "blue":0}
    for result in game_set.split(","):
        for color in set_results.keys():
            if color in result:
                set_results[color] = int(result[:-(len(color)+1)])
    print(f"single_set: {set_results}")
    return set_results

def split_game_into_three_sets(game):
    return game.split(";")

def convert_line_to_power(line):
    meta_set = {"red": 0, "green": 0, "blue": 0}
    for game_set in split_game_into_three_sets(line.split(":")[1]):
        parsed_game_set = parse_single_set(game_set)
        for key, value in parsed_game_set.items():
            if parsed_game_set[key] > meta_set[key]:
                meta_set[key] = parsed_game_set[key]

    power = 1
    for value in meta_set.values():
        power = power * value

    print(f"meta_set: {meta_set}")
    print(f"power: {power}")

    return power

cumulative_power_example = 0
with open("example-p2") as example_p2:
    for line in example_p2:
        cumulative_power_example += convert_line_to_power(line)


cumulative_power = 0
with open("input-p1") as input_p1:
    for line in input_p1:
        cumulative_power += convert_line_to_power(line)

print(f"example: {cumulative_power_example}")
print(f"p2: {cumulative_power}")
