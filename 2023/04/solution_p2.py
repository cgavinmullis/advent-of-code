import argparse
import logging

# Configure logging to output to both console and a file
logging.basicConfig(
    level=logging.DEBUG,
    format='%(asctime)s [%(levelname)s] %(message)s',
    handlers=[
        logging.FileHandler("output.log"),
        logging.StreamHandler()
    ]
)

class Card:
    def __init__(self, line):
        self.line = line
        self.number = int(line.split(":")[0].split(" ")[1])
        logging.debug(f"number: {self.number}")
        self.winning_numbers = self._clean_list(line.split(":")[1].split("|")[0].split(" "))
        logging.debug(f"winning_numbers: {self.winning_numbers}")
        self.numbers_you_have = self._clean_list(line.split(":")[1].split("|")[1].split(" "))
        logging.debug(f"numbers_you_have: {self.numbers_you_have}")
        self.number_of_matches = 0

    def _clean_list(self, dirty_list):
        clean_list = []
        for num in dirty_list:
            if num != "":
                clean_list.append(num)
        return clean_list


    def get_card_number(self):
        return self.number
    
    def get_winning_numbers(self):
        return self.winning_numbers

    def get_numbers_you_have(self):
        return self.numbers_you_have

    def calculate_matches(self):
        first_point = True
        for number in self.numbers_you_have:
            if number in self.winning_numbers:
                self.number_of_matches += 1

    def get_number_of_matches(self):
        return self.number_of_matches
    
def add_dupes(duplicator, card_matches, current_card_number):
    for i in range(card_matches):
        duplicator[current_card_number+i+1] += 1

    return duplicator

def main():
    # Set up command line argument parsing
    parser = argparse.ArgumentParser(description='Process lines from an input file.')
    parser.add_argument('input_file', type=str, help='Path to the input file')

    args = parser.parse_args()

    logging.info(f"Reading lines from file: {args.input_file}")

    card_pile = []
    duplicator = [0]*250

    try:
        # Open the input file
        with open(args.input_file, 'r') as file:
            # Read and process each line
            for index, line in enumerate(file):
                card = Card(line.rstrip())
                card.calculate_matches()
                card_pile.append(card)

                current_card_number = card.get_card_number()
                card_matches = card.get_number_of_matches()
                logging.debug(f"current_card_number: {current_card_number}")
                logging.debug(f"card_matches: {card_matches}")

                # Add dupes
                duplicator = add_dupes(duplicator, card_matches, current_card_number)

                # Calculate dupes for current card
                for dup in range(duplicator[current_card_number]):
                    card_pile.append(card)
                    duplicator = add_dupes(duplicator, card_matches, current_card_number)


    except FileNotFoundError:
        logging.error(f"File not found: {args.input_file}")

    logging.info(f"total: {len(card_pile)}")
if __name__ == "__main__":
    main()
