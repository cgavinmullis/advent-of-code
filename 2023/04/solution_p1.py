import argparse
import logging

# Configure logging to output to both console and a file
logging.basicConfig(
    level=logging.DEBUG,
    format='%(asctime)s [%(levelname)s] %(message)s',
    handlers=[
        logging.FileHandler("output.log"),
        logging.StreamHandler()
    ]
)

class Card:
    def __init__(self, line):
        self.line = line
        self.number = line.split(":")[0].split(" ")[1]
        logging.debug(f"number: {self.number}")
        self.winning_numbers = self._clean_list(line.split(":")[1].split("|")[0].split(" "))
        logging.debug(f"winning_numbers: {self.winning_numbers}")
        self.numbers_you_have = self._clean_list(line.split(":")[1].split("|")[1].split(" "))
        logging.debug(f"numbers_you_have: {self.numbers_you_have}")
        self.points = 0
        self.number_of_matches = 0

    def _clean_list(self, dirty_list):
        clean_list = []
        for num in dirty_list:
            if num != "":
                clean_list.append(num)
        return clean_list


    def get_card_number(self):
        return self.number
    
    def get_winning_numbers(self):
        return self.winning_numbers

    def get_numbers_you_have(self):
        return self.numbers_you_have

    def calculate_points(self):
        first_point = True
        for number in self.numbers_you_have:
            if number in self.winning_numbers:
                logging.debug(f"winning number: {number}")
                if first_point:
                    self.points = 1
                    first_point = False
                else:
                    self.points = self.points * 2
                logging.debug(f"new total: {self.points}")
                self.number_of_matches += 1

    def get_points(self):
        return self.points

    def get_number_of_matches(self):
        return self.number_of_matches
    


def main():
    # Set up command line argument parsing
    parser = argparse.ArgumentParser(description='Process lines from an input file.')
    parser.add_argument('input_file', type=str, help='Path to the input file')

    args = parser.parse_args()

    logging.info(f"Reading lines from file: {args.input_file}")

    card_pile = []
    cumulative_points = 0

    try:
        # Open the input file
        with open(args.input_file, 'r') as file:
            # Read and process each line
            for line in file:
                card = Card(line.rstrip())
                card.calculate_points()
                card_pile.append(card)
                cumulative_points += card.get_points()
    except FileNotFoundError:
        logging.error(f"File not found: {args.input_file}")

    logging.info(f"total: {cumulative_points}")
if __name__ == "__main__":
    main()
