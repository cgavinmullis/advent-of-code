import argparse
import logging

# Configure logging to output to both console and a file
logging.basicConfig(
    level=logging.DEBUG,
    format='%(asctime)s [%(levelname)s] %(message)s',
    handlers=[
        logging.FileHandler("output.log"),
        logging.StreamHandler()
    ]
)

def symbol_is_adjacent_left(start_index, num, line):
    # Check the left hand side
    if start_index <= 0:
        # There are no chars left of the number
        return False
    start_index = start_index - 1
    if line[start_index] != ".":
        # This must be a symbol then
        logging.info(f"symbol found to left: {line[start_index]}")
        logging.info(f"num is valid: {num}")
        return True
    return False

def symbol_is_adjacent_right(stop_index, num, line):
    # Check the right hand side
    if stop_index+1 > len(line)-1:
        # There are npoo chars to the right of the number
        return False
    stop_index = stop_index + 1
    if line[stop_index] != ".":
        # This must be a symbol then
        logging.info(f"symbol found to left: {line[stop_index]}")
        logging.info(f"num is valid: {num}")
        return True
    return False


def find_numbers_adjacent_to_symbols(raw_lines, numbers_in_lines):
    valid_nums = []
    for index, num_list in enumerate(numbers_in_lines):
        if not num_list:
            continue
        raw_line = raw_lines[index]
        logging.info(f"raw line stand: {raw_line}")
        start_index = 0
        stop_index = 0
        for num in num_list:
            # Check current line
            if start_index != 0:
                start_index = raw_line.find(num, stop_index +1)
            else:
                start_index = raw_line.find(num)
            stop_index = start_index + len(num) - 1
            logging.debug(f"start: {start_index}")
            logging.debug(f"stop: {stop_index}")
            logging.debug(f"num to check: {num}")
            if symbol_is_adjacent_left(start_index, num, raw_line):
                valid_nums.append(int(num))
                continue
            # Check the right hand side
            if symbol_is_adjacent_right(stop_index, num, raw_line):
                valid_nums.append(int(num))
                continue
            # Check line above (EVEN DIAGONALLY WTFFF)
            if index > 0:
                raw_line_above = raw_lines[index-1]
                logging.info(f"raw line above: {raw_line_above}")
                # We are safe to check above
                if start_index < len(raw_line_above):
                    if start_index-1 < 0:
                        top_start_index = 0
                    else:
                        top_start_index = start_index - 1
                    for char in raw_line_above[top_start_index:stop_index+2]:
                        logging.debug(f"top_start: {top_start_index}")
                        logging.debug(f"top_stop: {stop_index + 2}")
                        logging.debug(f"char: {char}")
                        if char != "." and not char.isdigit():
                            # This must be a symbol then
                            logging.info(f"symbol found above: {char}")
                            logging.info(f"num is valid: {num}")
                            valid_nums.append(int(num))
                            break
            #Check line below (EVEN DIAGONALLY)
            if index < len(raw_lines)-1:
                raw_line_below = raw_lines[index+1]
                logging.info(f"raw line below: {raw_line_below}")
                # We are safe to check below
                if start_index < len(raw_line_below):
                    if start_index-1 < 0:
                        bottom_start_index = 0
                    else:
                        bottom_start_index = start_index - 1
                    for char in raw_line_below[bottom_start_index:stop_index+2]:
                        if char != "." and not char.isdigit():
                            # This must be a symbol then
                            logging.info(f"symbol found below: {char}")
                            logging.info(f"num is valid: {num}")
                            valid_nums.append(int(num))
                            break
    return valid_nums

def get_numbers_in_line(line):
    logging.info(f"Processing line: {line.strip()}")
    delimiters = ['!', '"', '#', '$', '%', '&', "'", '(', ')',
                  '*', '+', ',', '-', '/', ':', ';', '<', ".",
                  '=', '>', '?', '@', '[', ']', '^', '_', '`',
                  '{', '|', '}', '~']
    for delimiter in delimiters:
        line = " ".join(line.split(delimiter))
    line = line.split()
    logging.info(f"parsed line: {line}")
    return line

def main():
    # Set up command line argument parsing
    parser = argparse.ArgumentParser(description='Process lines from an input file.')
    parser.add_argument('input_file', type=str, help='Path to the input file')

    args = parser.parse_args()

    logging.info(f"Reading lines from file: {args.input_file}")

    numbers_in_lines = []
    raw_lines = []
    try:
        # Open the input file
        with open(args.input_file, 'r') as file:
            # Read and process each line
            for line in file:
                numbers_in_lines.append(get_numbers_in_line(line.rstrip()))
                raw_lines.append(line.rstrip())
            logging.info(f"possible numbers: {numbers_in_lines}")
            numbers_adjacent_to_symbols = find_numbers_adjacent_to_symbols(raw_lines, numbers_in_lines)
            #for line in raw_lines:
            #    logging.info(f"{line}")
            #logging.info(f"numbers adjacent to symbols: {numbers_adjacent_to_symbols[-100:]}")
    except FileNotFoundError:
        logging.error(f"File not found: {args.input_file}")

    logging.info(sum(numbers_adjacent_to_symbols))
if __name__ == "__main__":
    main()
