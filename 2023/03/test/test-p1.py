# tests/find_numbers_adjacent_to_symbols

# tests/test_solution_p1.py

import sys
import os
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from solution_p1 import *

import argparse
import logging
import pytest
import logging

# Configure logging to output to both console and a file
@pytest.fixture(autouse=True)
def configure_logging():
    logging.basicConfig(
        level=logging.INFO,
        format='%(asctime)s [%(levelname)s] %(message)s',
        handlers=[
            logging.FileHandler("output.log"),
            logging.StreamHandler()
        ]
    )

def test_no_symbol():
    schematic = [
            "...",
            ".1.",
            "...",
            ]
    raw_nums = [[], ["1"], []]
    valid_nums = []
    assert find_numbers_adjacent_to_symbols(schematic, raw_nums) == valid_nums

def test_left_adjacent():
    schematic = [
            "...",
            "*1.",
            "...",
            ]
    raw_nums = [[], ["1"], []]
    valid_nums = [1]
    assert find_numbers_adjacent_to_symbols(schematic, raw_nums) == valid_nums

def test_right_adjacent():
    schematic = [
            "...",
            ".1*",
            "...",
            ]
    raw_nums = [[], ["1"], []]
    valid_nums = [1]
    assert find_numbers_adjacent_to_symbols(schematic, raw_nums) == valid_nums
def test_upper_left():
    schematic = [
            "*..",
            ".1.",
            "...",
            ]
    raw_nums = [[], ["1"], []]
    valid_nums = [1]
    assert find_numbers_adjacent_to_symbols(schematic, raw_nums) == valid_nums
def test_upper_middle():
    schematic = [
            ".*.",
            ".1.",
            "...",
            ]
    raw_nums = [[], ["1"], []]
    valid_nums = [1]
    assert find_numbers_adjacent_to_symbols(schematic, raw_nums) == valid_nums
def test_upper_right():
    schematic = [
            "..*",
            ".1.",
            "...",
            ]
    raw_nums = [[], ["1"], []]
    valid_nums = [1]
    assert find_numbers_adjacent_to_symbols(schematic, raw_nums) == valid_nums
def test_bottom_left():
    schematic = [
            "...",
            ".1.",
            "*..",
            ]
    raw_nums = [[], ["1"], []]
    valid_nums = [1]
    assert find_numbers_adjacent_to_symbols(schematic, raw_nums) == valid_nums
def test_bottom_middle():
    schematic = [
            "...",
            ".1.",
            ".*.",
            ]
    raw_nums = [[], ["1"], []]
    valid_nums = [1]
    assert find_numbers_adjacent_to_symbols(schematic, raw_nums) == valid_nums

def test_bottom_right():
    schematic = [
            "...",
            ".1.",
            "..*",
            ]
    raw_nums = [[], ["1"], []]
    valid_nums = [1]
    assert find_numbers_adjacent_to_symbols(schematic, raw_nums) == valid_nums

#Generated
def test_valid_number_in_top_left():
    schematic = [
        "1..",
        "*..",
        "...",
    ]
    raw_nums = [["1"], [], []]
    valid_nums = [1]
    assert find_numbers_adjacent_to_symbols(schematic, raw_nums) == valid_nums

def test_invalid_number_in_top_left():
    schematic = [
        "1..",
        "..*",
        "...",
    ]
    raw_nums = [["1"], [], []]
    valid_nums = []
    assert find_numbers_adjacent_to_symbols(schematic, raw_nums) == valid_nums

def test_valid_number_in_top_middle():
    schematic = [
        ".1.",
        "*..",
        "...",
    ]
    raw_nums = [["1"], [], []]
    valid_nums = [1]
    assert find_numbers_adjacent_to_symbols(schematic, raw_nums) == valid_nums

def test_valid_number_in_top_right():
    schematic = [
        "..1",
        "..*",
        "...",
    ]
    raw_nums = [["1"], [], []]
    valid_nums = [1]
    assert find_numbers_adjacent_to_symbols(schematic, raw_nums) == valid_nums


def test_invalid_number_in_top_right():
    schematic = [
        "..1",
        "*..",
        "...",
    ]
    raw_nums = [["1"], [], []]
    valid_nums = []
    assert find_numbers_adjacent_to_symbols(schematic, raw_nums) == valid_nums

def test_valid_number_in_middle_left():
    schematic = [
        "...",
        "1*.",
        "...",
    ]
    raw_nums = [[], ["1"], []]
    valid_nums = [1]
    assert find_numbers_adjacent_to_symbols(schematic, raw_nums) == valid_nums

def test_invalid_number_in_middle_right():
    schematic = [
        "...",
        "..1",
        "*..",
    ]
    raw_nums = [[], ["1"], []]
    valid_nums = []
    assert find_numbers_adjacent_to_symbols(schematic, raw_nums) == valid_nums

def test_valid_number_in_bottom_left():
    schematic = [
        "...",
        "*..",
        "1..",
    ]
    raw_nums = [[], [], ["1"]]
    valid_nums = [1]
    assert find_numbers_adjacent_to_symbols(schematic, raw_nums) == valid_nums

def test_invalid_number_in_bottom_left():
    schematic = [
        "...",
        "..*",
        "1..",
    ]
    raw_nums = [[], [], ["1"]]
    valid_nums = []
    assert find_numbers_adjacent_to_symbols(schematic, raw_nums) == valid_nums


def test_valid_number_in_bottom_middle():
    schematic = [
        "...",
        "*..",
        ".1.",
    ]
    raw_nums = [[], [], ["1"]]
    valid_nums = [1]
    assert find_numbers_adjacent_to_symbols(schematic, raw_nums) == valid_nums

def test_invalid_number_in_bottom_right():
    schematic = [
        "...",
        "*..",
        "..1",
    ]
    raw_nums = [[], [], ["1"]]
    valid_nums = []
    assert find_numbers_adjacent_to_symbols(schematic, raw_nums) == valid_nums

def test_multi_number_in_top_row():
    schematic = [
        "1.1",
        "*..",
        "...",
    ]
    raw_nums = [["1"], [], []]
    valid_nums = [1]
    assert find_numbers_adjacent_to_symbols(schematic, raw_nums) == valid_nums

def test_sub_string_fuckery():
    schematic = [
            "........",
            ".24..4..",
            "......*.",
            ]
    raw_nums = [[], ["24","4"], []]
    valid_nums = [4]
    assert find_numbers_adjacent_to_symbols(schematic, raw_nums) == valid_nums
