import argparse
import logging

# Configure logging to output to both console and a file
logging.basicConfig(
    level=logging.DEBUG,
    format='%(asctime)s [%(levelname)s] %(message)s',
    handlers=[
        logging.FileHandler("output.log"),
        logging.StreamHandler()
    ]
)

def which_part_number(numbers_adjacent_to_symbols, found_index, meta_line_index):
    for num, line_index, start_index, stop_index in numbers_adjacent_to_symbols:
        if line_index == meta_line_index:
            # This is the line we care about
            if start_index <= found_index <= stop_index:
                # This is the number
                logging.info(f"found part num: {num}")
                return num

def part_is_at_index(index, numbers_adjacent_to_symbols, line, line_index):
    if line[index].isdigit():
        # This must be a part number then
        logging.info(f"digit found: {line[index]}")
        return True, which_part_number(numbers_adjacent_to_symbols, index, line_index)
    return False, None

def part_is_adjacent_left(start_index, numbers_adjacent_to_symbols, line, line_index):
    # Check the left hand side
    if start_index <= 0:
        # There are no chars left of the star
        return False, None
    start_index = start_index - 1
    return part_is_at_index(start_index, numbers_adjacent_to_symbols, line, line_index)

def part_is_adjacent_right(stop_index, numbers_adjacent_to_symbols, line, line_index):

    # Check the left hand side
    if stop_index+1 > len(line)-1:
        # There are no chars left of the star
        return False, None
    stop_index = stop_index + 1
    return part_is_at_index(stop_index, numbers_adjacent_to_symbols, line, line_index)

def part_is_above_or_below(star_index, numbers_adjacent_to_symbols, line, line_index):

    parts = []

    # check middle
    is_adj, num = part_is_at_index(star_index, numbers_adjacent_to_symbols, line, line_index)
    if is_adj:
        parts.append(num)

    if parts:
        return True, parts

    # check diag left
    is_adj, num = part_is_adjacent_left(star_index, numbers_adjacent_to_symbols, line, line_index)
    if is_adj:
        parts.append(num)

    # check diag right
    is_adj, num = part_is_adjacent_right(star_index, numbers_adjacent_to_symbols, line, line_index)
    if is_adj:
        parts.append(num)
    
    if parts:
        return True, parts
    return False, None

def find_gear_ratios(numbers_adjacent_to_symbols, possible_gears, raw_lines):

    gear_ratios = []
    for index, star_list in enumerate(possible_gears):
        if not star_list:
            continue

        raw_line = raw_lines[index]
        logging.info(f"raw line stars: {raw_line}")

        start_index = 0
        stop_index = 0
        for star in star_list:

            # Get inclusive start/stop index
            if start_index != 0:
                start_index = raw_line.find(star, stop_index+1)
            else:
                start_index = raw_line.find(star)
            stop_index = start_index + len(star) - 1
            logging.debug(f"start: {start_index}")
            logging.debug(f"stop: {stop_index}")

            # Parts
            adjacent_part_numbers = []

            # Check the right hand side
            is_adj, part_num = part_is_adjacent_left(start_index, numbers_adjacent_to_symbols, raw_line, index)
            if is_adj:
                adjacent_part_numbers.append(int(part_num))
                
            # Check the right hand side
            is_adj, part_num = part_is_adjacent_right(stop_index, numbers_adjacent_to_symbols, raw_line, index)
            if is_adj:
                adjacent_part_numbers.append(int(part_num))

            # Check line above
            if index > 0:
                raw_line_above = raw_lines[index-1]
                logging.info(f"raw line above: {raw_line_above}")
                # We are safe to check above
                is_adj, part_nums = part_is_above_or_below(start_index, numbers_adjacent_to_symbols, raw_line_above, index-1)
                if is_adj:
                    for part_num in part_nums:
                        logging.info(part_num)
                        adjacent_part_numbers.append(int(part_num))

            # Check line below
            if index < len(raw_lines)-1:
                raw_line_below = raw_lines[index+1]
                logging.info(f"raw line below: {raw_line_below}")
                # We are safe to check below
                is_adj, part_nums = part_is_above_or_below(start_index, numbers_adjacent_to_symbols, raw_line_below, index+1)
                if is_adj:
                    logging.info(is_adj)
                    logging.info(part_nums)
                    for part_num in part_nums:
                        logging.info(part_num)
                        adjacent_part_numbers.append(int(part_num))

            if len(adjacent_part_numbers)== 2:
                logging.info(adjacent_part_numbers)
                gear_ratios.append(adjacent_part_numbers[0]*adjacent_part_numbers[1])
    return gear_ratios
            
def find_stars_in_line(line):
    logging.info(f"Processing line: {line.strip()}")
    stars = []
    index = line.strip().find("*")
    while index != -1:
        stars.append("*")
        index = line.strip().find("*", index + 1)
    logging.info(f"parsed line: {stars}")
    return stars

def symbol_is_adjacent_left(start_index, num, line):
    # Check the left hand side
    if start_index <= 0:
        # There are no chars left of the number
        return False
    start_index = start_index - 1
    if line[start_index] != ".":
        # This must be a symbol then
        logging.info(f"symbol found to left: {line[start_index]}")
        logging.info(f"num is valid: {num}")
        return True
    return False

def symbol_is_adjacent_right(stop_index, num, line):
    # Check the right hand side
    if stop_index+1 > len(line)-1:
        # There are npoo chars to the right of the number
        return False
    stop_index = stop_index + 1
    if line[stop_index] != ".":
        # This must be a symbol then
        logging.info(f"symbol found to left: {line[stop_index]}")
        logging.info(f"num is valid: {num}")
        return True
    return False


def find_numbers_adjacent_to_symbols(raw_lines, numbers_in_lines):
    valid_nums = []
    for index, num_list in enumerate(numbers_in_lines):
        if not num_list:
            continue
        raw_line = raw_lines[index]
        logging.info(f"raw line stand: {raw_line}")
        start_index = 0
        stop_index = 0
        for num in num_list:
            # Check current line
            if start_index != 0:
                start_index = raw_line.find(num, stop_index +1)
            else:
                start_index = raw_line.find(num)
            stop_index = start_index + len(num) - 1
            logging.debug(f"start: {start_index}")
            logging.debug(f"stop: {stop_index}")
            logging.debug(f"num to check: {num}")
            if symbol_is_adjacent_left(start_index, num, raw_line):
                valid_nums.append((int(num), index, start_index, stop_index))
                continue
            # Check the right hand side
            if symbol_is_adjacent_right(stop_index, num, raw_line):
                valid_nums.append((int(num), index, start_index, stop_index))
                continue
            # Check line above (EVEN DIAGONALLY WTFFF)
            if index > 0:
                raw_line_above = raw_lines[index-1]
                logging.info(f"raw line above: {raw_line_above}")
                # We are safe to check above
                if start_index < len(raw_line_above):
                    if start_index-1 < 0:
                        top_start_index = 0
                    else:
                        top_start_index = start_index - 1
                    for char in raw_line_above[top_start_index:stop_index+2]:
                        logging.debug(f"top_start: {top_start_index}")
                        logging.debug(f"top_stop: {stop_index + 2}")
                        logging.debug(f"char: {char}")
                        if char != "." and not char.isdigit():
                            # This must be a symbol then
                            logging.info(f"symbol found above: {char}")
                            logging.info(f"num is valid: {num}")
                            valid_nums.append((int(num), index, start_index, stop_index))
                            break
            #Check line below (EVEN DIAGONALLY)
            if index < len(raw_lines)-1:
                raw_line_below = raw_lines[index+1]
                logging.info(f"raw line below: {raw_line_below}")
                # We are safe to check below
                if start_index < len(raw_line_below):
                    if start_index-1 < 0:
                        bottom_start_index = 0
                    else:
                        bottom_start_index = start_index - 1
                    for char in raw_line_below[bottom_start_index:stop_index+2]:
                        if char != "." and not char.isdigit():
                            # This must be a symbol then
                            logging.info(f"symbol found below: {char}")
                            logging.info(f"num is valid: {num}")
                            valid_nums.append((int(num), index, start_index, stop_index))
                            break
    return valid_nums

def get_numbers_in_line(line):
    logging.info(f"Processing line: {line.strip()}")
    delimiters = ['!', '"', '#', '$', '%', '&', "'", '(', ')',
                  '*', '+', ',', '-', '/', ':', ';', '<', ".",
                  '=', '>', '?', '@', '[', ']', '^', '_', '`',
                  '{', '|', '}', '~']
    for delimiter in delimiters:
        line = " ".join(line.split(delimiter))
    line = line.split()
    logging.info(f"parsed line: {line}")
    return line

def main():
    # Set up command line argument parsing
    parser = argparse.ArgumentParser(description='Process lines from an input file.')
    parser.add_argument('input_file', type=str, help='Path to the input file')

    args = parser.parse_args()

    logging.info(f"Reading lines from file: {args.input_file}")

    numbers_in_lines = []
    raw_lines = []
    try:
        # Open the input file
        with open(args.input_file, 'r') as file:
            # Read and process each line
            for line in file:
                numbers_in_lines.append(get_numbers_in_line(line.rstrip()))
                raw_lines.append(line.rstrip())
            logging.info(f"possible numbers: {numbers_in_lines}")
            numbers_adjacent_to_symbols = find_numbers_adjacent_to_symbols(raw_lines, numbers_in_lines)
    except FileNotFoundError:
        logging.error(f"File not found: {args.input_file}")

    possible_gears = []
    gear_ratios = []
    try:
        # Open the input file
        with open(args.input_file, 'r') as file:
            # Read and process each line
            for line in file:
                possible_gears.append(find_stars_in_line(line.rstrip()))
            logging.info(f"part numbers: {numbers_adjacent_to_symbols}")
            logging.info(f"possible gears: {possible_gears}")
            gear_ratios = find_gear_ratios(numbers_adjacent_to_symbols, possible_gears, raw_lines)
            logging.info(sum(gear_ratios))
                
    except FileNotFoundError:
        logging.error(f"File not found: {args.input_file}")
if __name__ == "__main__":
    main()
