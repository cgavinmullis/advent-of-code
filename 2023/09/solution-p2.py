import argparse
import logging
from itertools import pairwise

# Configure logging to output to both console and a file
logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[logging.FileHandler("output.log"), logging.StreamHandler()],
)

def get_next_number(numbers):
    logging.debug(f"numbers: {numbers}")
    if len(numbers) == 0:
        return 0
    if all(num == 0 for num in numbers):
        return numbers[0]
    
    below = [second - first for first, second in pairwise(numbers)]
    logging.debug(f"below: {below}")

    next_number = get_next_number(below)

    logging.debug(f"numbers: {numbers}")
    logging.debug(f"head: {numbers[0]} + {next_number}")
    
    return numbers[0] - next_number


def main():
    # Set up command line argument parsing
    parser = argparse.ArgumentParser(description="Process lines from an input file.")
    parser.add_argument("input_file", type=str, help="Path to the input file")

    args = parser.parse_args()

    logging.info(f"Reading lines from file: {args.input_file}")

    number_sets = []
    try:
        # Open the input file
        with open(args.input_file, "r") as file:
            # Read and process each line
            for index, line in enumerate(file):
                line = line.rstrip()
                logging.debug(f"reading line: {line}")
                numbers = [int(num) for num in line.split()]
                number_sets.append(numbers)
    except FileNotFoundError:
        logging.error(f"File not found: {args.input_file}")
    logging.info("")

    # Do work here
    answers = []
    for numbers in number_sets:
        history = get_next_number(numbers)
        logging.info(f"history: {history}")
        answers.append(history)
    logging.info(f"Answer: {sum(answers)}")


if __name__ == "__main__":
    main()